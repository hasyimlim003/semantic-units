rootProject.name = "semantic-units"

include(
    "semantic-units:application",
    "semantic-units:adapter-input-rest-spring-mvc",
    "semantic-units:adapter-output-in-memory",
    "semantic-units:adapter-output-spring-data-neo4j"
)
include(
    "building-blocks:application",
    "building-blocks:adapter-input-rest-spring-mvc",
    "building-blocks:adapter-output-in-memory",
    "building-blocks:adapter-output-spring-data-neo4j"
)
include(
    "identity-management:application",
    "identity-management:adapter-output-in-memory"
)
include(
    "common:application",
    "common:adapter-output-in-memory"
)
include("rest-api-server")
