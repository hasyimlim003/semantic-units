package eu.tib.orkg.common.adapter.output.inmemory

import eu.tib.orkg.auth.domain.User
import eu.tib.orkg.auth.spi.UserRepository
import eu.tib.orkg.common.adapter.output.inmemory.internal.InMemoryUserRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryUserRepositoryAdapter(
    private val repository: InMemoryUserRepository
) : UserRepository {
    override fun save(user: User) = repository.save(user)

    override fun findById(uuid: UUID): Optional<User> = repository.findById(uuid)

    override fun findByEmail(email: String): Optional<User> = repository.findByEmail(email)

    override fun nextIdentity() = repository.nextIdentity()

    override fun deleteAll() = repository.deleteAll()
}
