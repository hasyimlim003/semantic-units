package eu.tib.orkg.common.adapter.output.inmemory.internal

import eu.tib.orkg.auth.domain.User
import eu.tib.orkg.common.adapter.output.inmemory.InMemoryRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryUserRepository : InMemoryRepository<UUID, User>(
    idSelector = User::id,
    defaultComparator = compareBy(User::id)
) {
    fun findByEmail(email: String): Optional<User> =
        Optional.ofNullable(entities.values.find { it.email == email })

    override fun nextIdentity(): UUID {
        var id = UUID.randomUUID()
        while(id in entities.keys) {
            id = UUID.randomUUID()
        }
        return id
    }
}
