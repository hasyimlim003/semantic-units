package eu.tib.orkg.auth.application

import eu.tib.orkg.common.application.SimpleMessageException
import org.springframework.http.HttpStatus
import java.util.*

class UserNotFound(id: UUID) :
    SimpleMessageException(HttpStatus.NOT_FOUND, """User "$id" not found.""")

class UserAlreadyExists private constructor(
    override val message: String
) : SimpleMessageException(HttpStatus.FORBIDDEN, message) {

    companion object {
        fun withId(id: UUID) =
            UserAlreadyExists("""User with id "$id" already exists.""")

        fun withEmail(email: String) =
            UserAlreadyExists("""User with email "$email" already exists.""")
    }
}

class UserNotAuthorized :
    SimpleMessageException(HttpStatus.UNAUTHORIZED, """Not authorized.""")
