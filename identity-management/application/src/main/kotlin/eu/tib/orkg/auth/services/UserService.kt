package eu.tib.orkg.auth.services

import eu.tib.orkg.auth.api.CreateUserUseCase
import eu.tib.orkg.auth.api.UserUseCases
import eu.tib.orkg.auth.application.UserAlreadyExists
import eu.tib.orkg.auth.application.UserNotAuthorized
import eu.tib.orkg.auth.application.UserNotFound
import eu.tib.orkg.auth.domain.User
import eu.tib.orkg.auth.spi.UserRepository
import org.springframework.stereotype.Service
import java.security.Principal
import java.util.*

@Service
class UserService(
    private val repository: UserRepository
) : UserUseCases {
    override fun create(command: CreateUserUseCase.CreateCommand): UUID {
        if (command.id != null && repository.findById(command.id).isPresent) {
            throw UserAlreadyExists.withId(command.id)
        }
        val user = User(
            id = command.id ?: repository.nextIdentity(),
            username = command.username,
            email = command.email,
            password = command.password
        )
        repository.save(user)
        return user.id
    }

    override fun findById(uuid: UUID): Optional<User> =
        repository.findById(uuid)

    override fun findByEmail(email: String): Optional<User> =
        repository.findByEmail(email)

    override fun authenticateUser(principal: Principal?): User {
        if (principal?.name == null) throw UserNotAuthorized()
        val id = UUID.fromString(principal.name)
        return findById(id).orElseThrow { UserNotFound(id) }
    }

    override fun deleteAll() = repository.deleteAll()
}
