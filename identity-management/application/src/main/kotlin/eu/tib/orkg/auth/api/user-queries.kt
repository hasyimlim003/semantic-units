package eu.tib.orkg.auth.api

import eu.tib.orkg.auth.domain.User
import java.security.Principal
import java.util.*

interface RetrieveUserUseCases {
    fun findById(uuid: UUID): Optional<User>
    fun findByEmail(email: String): Optional<User>
}

interface AuthenticateUserUseCase {
    fun authenticateUser(principal: Principal?): User
}
