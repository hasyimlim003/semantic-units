package eu.tib.orkg.auth.api

interface UserUseCases : CreateUserUseCase, RetrieveUserUseCases, AuthenticateUserUseCase, DeleteUserUseCase
