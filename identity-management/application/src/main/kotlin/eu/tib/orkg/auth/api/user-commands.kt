package eu.tib.orkg.auth.api

import java.util.*

interface CreateUserUseCase {
    fun create(command: CreateCommand): UUID

    data class CreateCommand(
        val id: UUID?,
        val username: String,
        val email: String,
        val password: String
    )
}

interface DeleteUserUseCase {
    fun deleteAll()
}
