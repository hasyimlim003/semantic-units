package eu.tib.orkg.auth.spi

import eu.tib.orkg.auth.domain.User
import java.util.*

interface UserRepository {
    fun save(user: User)
    fun findById(uuid: UUID): Optional<User>
    fun findByEmail(email: String): Optional<User>
    fun nextIdentity(): UUID
    fun deleteAll()
}
