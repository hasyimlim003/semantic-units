package eu.tib.orkg.auth.domain

import java.util.*

data class User(
    val id: UUID,
    val username: String,
    val email: String,
    val password: String
)
