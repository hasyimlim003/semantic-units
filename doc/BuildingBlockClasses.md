# Endpoints for Building Block Classes

## Building Block Classes

**GET** /api/building-blocks/classes/{id}\
Description: Fetches a building block class\
Responses:
* <details><summary>200 OK</summary>

    ```json
    {
        "id": <building_block_uuid>,
        "label": <string>,
        "description": <string>,
        "type": <building_block_type>,
        "created_by": <uuid>,
        "created_at": <date_time>,
        "subject_constraint": <optional_uri>,
        "object_position_classes": [
            {
                "id": <object_position_uuid>,
                "label": <string>,
                "data_type": <data_type>,
                "required": <boolean>,
                "created_by": <uuid>,
                "created_at": <date_time>,
                "constraint": <constraint>
            },
            ...
        ]
    }
    ```
    </details> 
* 404 NOT FOUND

**GET** /api/building-blocks/classes\
Description: Fetches all building block classes
Responses:
* 200 OK - paged response body of /api/building-blocks/classes/{id}

**POST** /api/building-blocks/classes\
Description: Creates a new building block class\
Requires: Login
<details><summary>Request body</summary>

```json
{
    "label": <string>,
    "description": <string>,
    "type": <building_block_type>,
    "subject_constraint": <optional_uri>,
    "object_position_classes": [
        {
            "label": <string>,
            "data_type": <data_type>,
            "required": <boolean>,
            "constraint": <constraint>
        },
        ...
    ]
}
```
</details>

Responses:
* 201 CREATED - location header set to /api/building-blocks/classes/{id} with the same response body

## Data models

### Constraints
<details><summary>String</summary>

```json
{
    "pattern": <string>
}
```
</details>

<details><summary>Integer</summary>

```json
{
    "min": <int>,
    "max": <int>,
    "min_exclusive": <boolean>,
    "max_exclusive": <boolean>
}
```
</details>

<details><summary>Decimal</summary>

```json
{
    "min": <double>,
    "max": <double>,
    "min_exclusive": <boolean>,
    "max_exclusive": <boolean>
}
```
</details>

<details><summary>Resource</summary>

```json
{
    "ancestor": <uri>
}
```
</details>

<details><summary>Datetime</summary>

```json
{
    "min": <date_time>,
    "max": <date_time>,
    "min_exclusive": <boolean>,
    "max_exclusive": <boolean>
}
```
</details>

## Enums

### Building Block Types:
* `statement`

### Data Types:
* `string`
* `integer`
* `decimal`
* `resource`
* `boolean`
* `date_time`
