# Endpoints for Semantic Units

## Semantic Units

**GET** /api/semantic-units/{id}\
Description: Fetches a semantic unit\
Responses:
* <details><summary>200 OK</summary>

    Statement Unit:
    ```json
    {
        "id": <semantic_unit_uuid>,
        "subject": <resource_uuid or semantic_unit_uuid>,
        "building_block_id": <building_block_uuid>,
        "created_by": <uuid>,
        "created_at": <date_time>,
        "deleted_by": <uuid>,
        "deleted_at": <date_time>,
        "is_editable": <boolean>,
        "type": <statement_unit_type>,
        "carried_over_subject_range_constraints": {,
            <object_position_uuid>: <object_position_uuid>,
            ...
        },
        "object_nodes": [
            {
                "id": <object_node_uuid>,
                "object_position_class": <object_position_id>,
                "created_by": <uuid>,
                "created_at": <date_time>,
                "value": <string|boolean|long|double|date_time|uuid>
            },
            ...
        ]
    }
    ```
    
    Instance Identification Statement Unit:
    ```json
    {
        "id": <semantic_unit_uuid>,
        "subject": <resource_uuid or semantic_unit_uuid>,
        "building_block_id": <building_block_uuid>,
        "created_by": <uuid>,
        "created_at": <date_time>,
        "deleted_by": <uuid>,
        "deleted_at": <date_time>,
        "is_editable": <boolean>,
        "type": <statement_unit_type>,
        "instance_type": <instance_identification_unit_type>,
        "object_nodes": [
            {
                "id": <object_node_uuid>,
                "object_position_class": <object_position_id>,
                "created_by": <uuid>,
                "created_at": <date_time>,
                "value": <string|uuid>
            },
            ...
        ]
    }
    ```
    </details>

* 404 NOT FOUND

**GET** /api/semantic-units\
Description: Fetches all semantic units
Responses:
* 200 OK - paged response body of /api/semantic-units/{id}

**POST** /api/semantic-units\
Description: Creates a new semantic unit\
Requires: Login
<details><summary>Request body</summary>

```json
{
    "subject": <resource_input>,
    "building_block_instance_id": <building_block_instance_uuid>,
    "type": <optional_statement_unit_type>,
    "inputs": {
        "<object_position_uuid>": <object_position_input>,
        ...
    },
    "children": [
        <semantic_unit_input>,
        ...
    ]
}
```
#### Object Position Inputs
Resource Inputs

<details><summary>Existing Resource Input</summary>
    
```json
{
    "resource": <uuid>
}
```
    
</details>

<details><summary>New Resource Input</summary>

```json
{
    "class": <uri>,
    "label": <optional_string>,
    "type": <intance_identification_unit_type>
}
```

</details>

<details><summary>Statement Unit Resource Input</summary>

```json
{
    "statement": <semantic_unit_uuid>
}
```

</details>

Literal Inputs

<details><summary>Literal Input</summary>

```json
{
    "value": <string|long|double|boolean|date_time>
}
```

</details>

</details>

Responses:
* 201 CREATED - location header set to /api/semantic-units/{id} including the same response body

**POST** /api/semantic-units/{id}\
Description: Updates an existing semantic unit\
Requires: Login
<details><summary>Request body</summary>

```json
{
    "inputs": {
        "<object_position_uuid>": <object_position_input>,
        ...
    },
    "children": [
        <semantic_unit_input>,
        ...
    ]
}
```
See /api/semantic-units for more info
</details>

Responses:
* 200 OK
* 404 NOT FOUND
* 403 FORBIDDEN

## Enums

### Statement Unit Types:
* `assertional` An assertional statement is a statement that is true for a specific particular (i.e., individual) and specifies a fact. It thus states what is the case. Empirical data (i.e., measurements and observations), descriptions and reports are typical examples for assertional statements. In a knowledge graph, assertional statements relate particular instances (i.e., namedIndividuals) to each other.
* `contingent` A contingent statement is a statement that is true for some instances of a specific universal (i.e., class). It states what can be the case. In a knowledge graph, contingent statements are true for some, but not necessarily every instance of a class.
* `prototypical` A prototypical statement is a contingent statement that is true for most instances of a specific universal (i.e., class). It states what is likely or typically the case. Examples are statements about the relationships between diseases and their symptoms, or drugs and their effects, which are often expressed in reference to some probability evaluation, many empirical recognition criteria, or in general all things that are subject to exceptions. A prototypical statement is a statement that is true for all instances of a specific universal as long as not the contrary is explicitly stated.
* `universal` A universal statement is a statement that is true for every instance of a specific universal (i.e., class). A universal statement represents commonly accepted domain knowledge. It states what is necessarily the case. Definitions of ontology classes and their class axioms are examples of universal statements, but also many scientific hypotheses and law-like regularities take the form of universal statements.
* `lexical` A lexical statement is a statement that is about a linguistic item such as a term and comprises information such as the label of a given resource, who created it, from which ontology it has been imported, etc.

### Instance Identification Unit Types:
* `named_individual`
* `some_instance`
* `every_instance`
