# Endpoints for Building Block Instances

## Building Block Instances

**GET** /api/building-blocks/instances/{id}\
Description: Fetches a building block instance
Responses:
* <details><summary>200 OK</summary>
  
    ```json
    {
        "id": <building_block_instance_uuid>,
        "building_block_id": <building_block_uuid>,
        "links": [
            {
                "id": <link_uuid>
                "target_building_block_instance_id": <building_block_instance_uuid>,
                "min_count": <int>,
                "max_count": <int>,
                "created_by": <uuid>,
                "created_at": <date_time>,
                "use_as_subject": <object_position_uuid>
            },
            ...
        ],
        "associations": [
            {
                "id": <association_uuid>
                "target_building_block_instance_id": <building_block_instance_uuid>,
                "min_count": <int>,
                "max_count": <int>,
                "created_by": <uuid>,
                "created_at": <date_time>,
                "carry_over_subject_range_constraint_to": <object_position_uuid>
            },
            ...
        ],
        "references": [
            {
                "id": <association_uuid>
                "target_building_block_instance_id": <building_block_instance_uuid>,
                "min_count": <int>,
                "max_count": <int>,
                "created_by": <uuid>,
                "created_at": <date_time>
            },
            ...
        ]
    }
    ```
    </details>
* 404 NOT FOUND

**GET** /api/building-blocks/instances\
Description: Fetches all building block instances
Responses:
* 200 OK - paged response body of /api/building-blocks/instances/{id}

**POST** /api/building-blocks/instances\
Description: Creates a new building block instance\
Requires: Login
<details><summary>Request body</summary>

```json
{
    "building_block_id": <building_block_uuid>,
    "links": [
        <link_request>,
        ...
    ],
    "associations": [
        <association_request>,
        ...
    ],
    "references": [
        <reference_request>,
        ...
    ]
}
```
</details>

Responses:
* 201 CREATED - location header set to /api/building-blocks/instances/{id} with the same response body

### Links

**POST** /api/building-blocks/instances/{id}/links\
Description: Creates a new outgoing link for the building block instance\
Requires: Login
<details><summary>Request body</summary>

```json
{
    "target_building_block_instance_id": <building_block_instance_uuid>,
    "min_count": <int>,
    "max_count": <int>,
    "use_as_subject": <object_position_uuid>
}
```
</details>

Responses:
* 201 CREATED - location header set to /api/building-blocks/instances/{id} with the same response body
* 404 NOT FOUND
* 403 FORBIDDEN

### Associations

**POST** /api/building-blocks/instances/{id}/associations\
Description: Creates a new outgoing association for the building block instance\
Requires: Login
<details><summary>Request body</summary>

```json
{
    "target_building_block_instance_id": <building_block_instance_uuid>,
    "min_count": <int>,
    "max_count": <int>,
    "carry_over_subject_range_constraint_to": <object_position_uuid>
}
```
</details>

Responses:
* 201 CREATED - location header set to /api/building-blocks/instances/{id} with the same response body
* 404 NOT FOUND
* 403 FORBIDDEN

### References

**POST** /api/building-blocks/instances/{id}/references\
Description: Creates a new outgoing reference for the building block instance\
Requires: Login
<details><summary>Request body</summary>

```json
{
    "target_building_block_instance_id": <building_block_instance_uuid>,
    "min_count": <int>,
    "max_count": <int>
}
```
</details>

Responses:
* 201 CREATED - location header set to /api/building-blocks/instances/{id} with the same response body
* 404 NOT FOUND
* 403 FORBIDDEN
