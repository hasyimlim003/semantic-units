package eu.tib.orkg.common.adapter.output.inmemory

import eu.tib.orkg.common.spi.EntityRepository
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import java.util.*
import kotlin.Comparator

abstract class InMemoryRepository<ID, T: Any>(
    private val idSelector: (T) -> ID,
    private val defaultComparator: Comparator<T>
) : EntityRepository<ID, T> {
    protected val entities: MutableMap<ID, T> = mutableMapOf()

    override fun save(entity: T) {
        entities[idSelector(entity)] = entity
    }

    override fun findAll(pageable: Pageable) = entities.values
        .sortedWith(defaultComparator)
        .paged(pageable)

    override fun findById(id: ID): Optional<T> = Optional.ofNullable(entities[id])

    override fun deleteAll() = entities.clear()
}

fun <T> List<T>.paged(pageable: Pageable): PageImpl<T> {
    val content = this
        .drop(pageable.pageNumber * pageable.pageSize)
        .take(pageable.pageSize)
    return PageImpl(
        content,
        PageRequest.of(pageable.pageNumber, pageable.pageSize),
        this.size.toLong()
    )
}
