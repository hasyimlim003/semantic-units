package eu.tib.orkg.common.spi

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import java.util.*

interface EntityRepository<ID, T: Any> : IdRepository<ID> {
    fun save(entity: T)
    fun findAll(pageable: Pageable): Page<T>
    fun findById(id: ID): Optional<T>
    fun deleteAll()
}

/**
 * Performs the given `action` on each element in the repository.
 * Elements are loaded in chunks of size `chunkSize`.
 */
fun <ID, T: Any> EntityRepository<ID, T>.forEach(chunkSize: Int = 10_000, action: (T) -> Unit) {
    var page: Page<T> = findAll(PageRequest.of(0, chunkSize))
    page.forEach(action)
    while (page.hasNext()) {
        page = findAll(page.nextPageable())
        page.forEach(action)
    }
}
