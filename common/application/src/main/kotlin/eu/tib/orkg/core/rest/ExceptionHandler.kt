package eu.tib.orkg.core.rest

import eu.tib.orkg.common.application.SimpleMessageException
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(SimpleMessageException::class)
    fun handleSimpleMessageException(
        ex: SimpleMessageException,
        request: WebRequest
    ): ResponseEntity<Any> {
        val payload = MessageErrorResponse(
            status = ex.status.value(),
            error = ex.status.reasonPhrase,
            path = request.requestURI,
            message = ex.message
        )
        return ResponseEntity(payload, ex.status)
    }

    data class MessageErrorResponse(
        val status: Int,
        val error: String,
        val path: String,
        val timestamp: LocalDateTime = LocalDateTime.now(),
        val message: String?
    )

    private val WebRequest.requestURI: String
        get() = when (this) {
//            is ServletWebRequest -> request.requestURI
            else -> contextPath // most likely this is empty
        }
}
