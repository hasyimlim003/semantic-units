package eu.tib.orkg.common.application

import org.springframework.data.domain.PageRequest

object PageRequests {
    val ALL: PageRequest = PageRequest.of(0, Int.MAX_VALUE)
}
