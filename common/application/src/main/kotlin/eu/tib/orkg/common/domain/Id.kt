package eu.tib.orkg.common.domain

private val validIdRegex: Regex = """^[a-zA-Z0-9:_-]+$""".toRegex()

abstract class Id<T>(
    val value: String
) : Comparable<T> where T: Id<T> {

    init {
        require(value.isNotBlank()) { "ID must not be blank" }
        require(value.matches(validIdRegex)) { "ID must only contain alphanumeric characters, dashes and underscores" }
    }

    override fun compareTo(other: T) = value.compareTo(other.value)
}
