package eu.tib.orkg.common.application

import org.springframework.http.HttpStatus

abstract class SimpleMessageException(
    val status: HttpStatus,
    override val message: String,
    override val cause: Throwable? = null
) : RuntimeException(message, cause)
