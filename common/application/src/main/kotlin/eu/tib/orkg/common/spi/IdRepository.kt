package eu.tib.orkg.common.spi

interface IdRepository<T> {
    fun nextIdentity(): T
}
