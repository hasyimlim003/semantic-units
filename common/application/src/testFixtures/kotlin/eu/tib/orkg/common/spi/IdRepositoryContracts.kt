package eu.tib.orkg.common.spi

import io.kotest.core.spec.style.describeSpec
import io.kotest.matchers.shouldNotBe

fun <T> idRepositoryContract(
    repository: IdRepository<T>
) = describeSpec {
    context("requesting a new id from the repository") {
        it("returns a previously unseen unique id") {
            repository.nextIdentity() shouldNotBe repository.nextIdentity()
        }
    }
}
