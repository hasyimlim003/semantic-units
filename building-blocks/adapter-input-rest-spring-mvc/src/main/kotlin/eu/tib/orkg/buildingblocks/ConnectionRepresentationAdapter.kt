package eu.tib.orkg.buildingblocks

import eu.tib.orkg.buildingblocks.api.AssociationRepresentation
import eu.tib.orkg.buildingblocks.api.ConnectionRepresentation
import eu.tib.orkg.buildingblocks.api.LinkRepresentation
import eu.tib.orkg.buildingblocks.api.ReferenceRepresentation
import eu.tib.orkg.buildingblocks.domain.connection.Association
import eu.tib.orkg.buildingblocks.domain.connection.Connection
import eu.tib.orkg.buildingblocks.domain.connection.Link
import eu.tib.orkg.buildingblocks.domain.connection.Reference
import java.util.*

interface ConnectionRepresentationAdapter {
    fun Optional<Connection<*>>.mapToConnectionRepresentation(): Optional<ConnectionRepresentation<*>> =
        map { it.toConnectionRepresentation() }

    fun Connection<*>.toConnectionRepresentation(): ConnectionRepresentation<*> =
        when (this) {
            is Association -> toAssociationRepresentation()
            is Link -> toLinkRepresentation()
            is Reference -> toReferenceRepresentation()
        }

    fun Association.toAssociationRepresentation(): AssociationRepresentation =
        AssociationRepresentation(
            id = id,
            targetBuildingBlockInstance = targetBuildingBlockInstance,
            minCount = minCount,
            maxCount = maxCount,
            createdBy = createdBy,
            createdAt = createdAt,
            carryOverSubjectRangeConstraints = carryOverSubjectRangeConstraints
        )

    fun Link.toLinkRepresentation(): LinkRepresentation =
        LinkRepresentation(
            id = id,
            targetBuildingBlockInstance = targetBuildingBlockInstance,
            minCount = minCount,
            maxCount = maxCount,
            createdBy = createdBy,
            createdAt = createdAt,
            useAsSubject = useAsSubject
        )

    fun Reference.toReferenceRepresentation(): ReferenceRepresentation =
        ReferenceRepresentation(
            id = id,
            targetBuildingBlockInstance = targetBuildingBlockInstance,
            minCount = minCount,
            maxCount = maxCount,
            createdBy = createdBy,
            createdAt = createdAt,
        )
}
