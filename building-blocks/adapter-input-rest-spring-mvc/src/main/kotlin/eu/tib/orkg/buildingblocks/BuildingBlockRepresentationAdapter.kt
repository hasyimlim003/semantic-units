package eu.tib.orkg.buildingblocks

import eu.tib.orkg.buildingblocks.api.BuildingBlockRepresentation
import eu.tib.orkg.buildingblocks.domain.BuildingBlock
import org.springframework.data.domain.Page
import java.util.*

interface BuildingBlockRepresentationAdapter : ObjectPositionRepresentationAdapter {
    fun Optional<BuildingBlock>.mapToBuildingBlockRepresentation(): Optional<BuildingBlockRepresentation> =
        map { it.toBuildingBlockRepresentation() }

    fun Page<BuildingBlock>.mapToBuildingBlockRepresentation(): Page<BuildingBlockRepresentation> =
        map { it.toBuildingBlockRepresentation() }

    private fun BuildingBlock.toBuildingBlockRepresentation(): BuildingBlockRepresentation =
        BuildingBlockRepresentation(
            id = id,
            label = label,
            description = description,
            type = type,
            createdBy = createdBy,
            createdAt = createdAt,
            subjectConstraint = subjectConstraint,
            objectPositionClasses = objectPositionClasses.mapValues { it.value.toObjectPositionRepresentation() }
        )
}
