package eu.tib.orkg.buildingblocks.application

import com.fasterxml.jackson.annotation.JsonProperty
import eu.tib.orkg.auth.api.UserUseCases
import eu.tib.orkg.buildingblocks.BuildingBlockInstanceRepresentationAdapter
import eu.tib.orkg.buildingblocks.api.*
import eu.tib.orkg.buildingblocks.domain.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/api/building-blocks/instances", produces = [MediaType.APPLICATION_JSON_VALUE])
class BuildingBlockInstanceController(
    private val service: BuildingBlockInstanceUseCases,
    private val userService: UserUseCases
) : BuildingBlockInstanceRepresentationAdapter {
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun create(
        @RequestBody request: CreateBuildingBlockInstanceRequest,
        principal: Principal?,
        uriComponentsBuilder: UriComponentsBuilder
    ): ResponseEntity<BuildingBlockInstanceRepresentation> {
        val user = userService.authenticateUser(principal)
        val command = request.toCreateCommand(ContributorId(user.id))
        val id = service.create(command)
        val location = uriComponentsBuilder
            .path("api/building-blocks/instances/{id}") // TODO: check if path needs a leading '/'
            .buildAndExpand(id)
            .toUri()
        return created(location).body(service.findById(id).mapToBuildingBlockInstanceRepresentation().get())
    }

    @PostMapping("/{id}/associations")
    fun createAssociation(
        @PathVariable id: BuildingBlockInstanceId,
        @RequestBody request: CreateAssociationRequest,
        principal: Principal?,
        uriComponentsBuilder: UriComponentsBuilder
    ): ResponseEntity<Any> {
        val user = userService.authenticateUser(principal)
        val command = request.toCreateCommand(ContributorId(user.id), id)
        val associationId = service.createAssociation(command)
        val location = uriComponentsBuilder
            .path("api/building-blocks/instances/{id}") // TODO: check if path needs a leading '/'
            .buildAndExpand(associationId)
            .toUri()
        return created(location).build() // TODO: empty body?
    }

    @PostMapping("/{id}/links")
    fun createLink(
        @PathVariable id: BuildingBlockInstanceId,
        @RequestBody request: CreateLinkRequest,
        principal: Principal?,
        uriComponentsBuilder: UriComponentsBuilder
    ): ResponseEntity<Any> {
        val user = userService.authenticateUser(principal)
        val command = request.toCreateCommand(ContributorId(user.id), id)
        val linkId = service.createLink(command)
        val location = uriComponentsBuilder
            .path("api/building-blocks/instances/{id}") // TODO: check if path needs a leading '/'
            .buildAndExpand(linkId)
            .toUri()
        return created(location).build() // TODO: empty body?
    }

    @PostMapping("/{id}/references")
    fun createReference(
        @PathVariable id: BuildingBlockInstanceId,
        @RequestBody request: CreateReferenceRequest,
        principal: Principal?,
        uriComponentsBuilder: UriComponentsBuilder
    ): ResponseEntity<Any> {
        val user = userService.authenticateUser(principal)
        val command = request.toCreateCommand(ContributorId(user.id), id)
        val referenceId = service.createReference(command)
        val location = uriComponentsBuilder
            .path("api/building-blocks/instances/{id}") // TODO: check if path needs a leading '/'
            .buildAndExpand(referenceId)
            .toUri()
        return created(location).build() // TODO: empty body?
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: BuildingBlockInstanceId): BuildingBlockInstanceRepresentation =
        service.findById(id)
            .mapToBuildingBlockInstanceRepresentation()
            .orElseThrow { BuildingBlockInstanceNotFound(id) }

    @GetMapping
    fun findAll(pageable: Pageable): Page<BuildingBlockInstanceRepresentation> =
        service.findAll(pageable).mapToBuildingBlockInstanceRepresentation()

    data class CreateBuildingBlockInstanceRequest(
        @JsonProperty("building_block_id")
        val buildingBlockId: BuildingBlockId,
        val associations: Set<CreateAssociationRequest>,
        val links: Set<CreateLinkRequest>,
        val references: Set<CreateReferenceRequest>
    ) {
        fun toCreateCommand(contributorId: ContributorId) = CreateBuildingBlockInstanceUseCase.CreateCommand(
            buildingBlockId = buildingBlockId,
            contributorId = contributorId,
            associations = associations.map { it.toCreateCommand() }.toSet(),
            links = links.map { it.toCreateCommand() }.toSet(),
            references = references.map { it.toCreateCommand() }.toSet(),
        )
    }

    data class CreateAssociationRequest(
        @JsonProperty("target_building_block_instance_id")
        val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        @JsonProperty("min_count")
        val minCount: Int,
        @JsonProperty("max_count")
        val maxCount: Int,
        @JsonProperty("carry_over_subject_range_constraint_to")
        val carryOverSubjectRangeConstraintTo: ObjectPositionId?
    ) {
        fun toCreateCommand() = CreateBuildingBlockInstanceUseCase.CreateCommand.Association(
            targetBuildingBlockInstanceId = targetBuildingBlockInstanceId,
            minCount = minCount,
            maxCount = maxCount,
            carryOverSubjectRangeConstraints = Optional.ofNullable(carryOverSubjectRangeConstraintTo)
                .map { setOf(it) }
                .orElseGet { setOfNotNull() }
        )

        fun toCreateCommand(contributorId: ContributorId, id: BuildingBlockInstanceId) =
            CreateAssociationUseCase.CreateAssociationCommand(
                contributorId = contributorId,
                sourceBuildingBlockInstanceId = id,
                targetBuildingBlockInstanceId = targetBuildingBlockInstanceId,
                minCount = minCount,
                maxCount = maxCount,
                carryOverSubjectRangeConstraints = Optional.ofNullable(carryOverSubjectRangeConstraintTo)
                    .map { setOf(it) }
                    .orElseGet { setOf() }
            )
    }

    data class CreateLinkRequest(
        @JsonProperty("target_building_block_instance_id")
        val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        @JsonProperty("min_count")
        val minCount: Int,
        @JsonProperty("max_count")
        val maxCount: Int,
        @JsonProperty("use_as_subject")
        val useAsSubject: ObjectPositionId
    ) {
        fun toCreateCommand() = CreateBuildingBlockInstanceUseCase.CreateCommand.Link(
            targetBuildingBlockInstanceId = targetBuildingBlockInstanceId,
            minCount = minCount,
            maxCount = maxCount,
            useAsSubject = useAsSubject
        )

        fun toCreateCommand(contributorId: ContributorId, id: BuildingBlockInstanceId) =
            CreateLinkUseCase.CreateLinkCommand(
                contributorId = contributorId,
                sourceBuildingBlockInstanceId = id,
                targetBuildingBlockInstanceId = targetBuildingBlockInstanceId,
                minCount = minCount,
                maxCount = maxCount,
                useAsSubject = useAsSubject
            )
    }

    data class CreateReferenceRequest(
        @JsonProperty("target_building_block_instance_id")
        val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        @JsonProperty("min_count")
        val minCount: Int,
        @JsonProperty("max_count")
        val maxCount: Int
    ) {
        fun toCreateCommand() = CreateBuildingBlockInstanceUseCase.CreateCommand.Reference(
            targetBuildingBlockInstanceId = targetBuildingBlockInstanceId,
            minCount = minCount,
            maxCount = maxCount
        )

        fun toCreateCommand(contributorId: ContributorId, id: BuildingBlockInstanceId) =
            CreateReferenceUseCase.CreateReferenceCommand(
                contributorId = contributorId,
                sourceBuildingBlockInstanceId = id,
                targetBuildingBlockInstanceId = targetBuildingBlockInstanceId,
                minCount = minCount,
                maxCount = maxCount
            )
    }
}
