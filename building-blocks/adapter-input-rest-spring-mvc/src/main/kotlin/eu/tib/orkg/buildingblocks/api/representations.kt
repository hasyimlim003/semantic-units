package eu.tib.orkg.buildingblocks.api

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import eu.tib.orkg.buildingblocks.domain.*
import java.net.URI
import java.time.LocalDateTime

data class BuildingBlockRepresentation(
    val id: BuildingBlockId,
    val label: String,
    val description: String,
    val type: BuildingBlock.Type,
    @JsonProperty("created_by")
    val createdBy: ContributorId,
    @JsonProperty("created_at")
    val createdAt: LocalDateTime,
    @JsonProperty("subject_constraint")
    val subjectConstraint: URI?,
    @JsonProperty("object_positions_classes")
    val objectPositionClasses: Map<ObjectPositionId, ObjectPositionRepresentation>
)

data class ObjectPositionRepresentation(
    val id: ObjectPositionId,
    val label: String,
    @JsonProperty("data_type")
    val dataType: DataType,
    val required: Boolean,
    @JsonInclude(JsonInclude.Include.NON_NULL)
    val constraint: ConstraintRepresentation?,
    @JsonProperty("created_by")
    val createdBy: ContributorId,
    @JsonProperty("created_at")
    val createdAt: LocalDateTime,
)

sealed interface ConstraintRepresentation

data class ComparableConstraintRepresentation<T>(
    val min: T,
    val max: T,
    @JsonProperty("min_exclusive")
    val minExclusive: Boolean,
    @JsonProperty("max_exclusive")
    val maxExclusive: Boolean
) : ConstraintRepresentation

data class StringConstraintRepresentation(
    val pattern: String
) : ConstraintRepresentation

data class ResourceConstraintRepresentation(
    val ancestor: URI
) : ConstraintRepresentation

data class BuildingBlockInstanceRepresentation(
    val id: BuildingBlockInstanceId,
    val buildingBlockId: BuildingBlockId,
    val createdBy: ContributorId,
    val createdAt: LocalDateTime,
    val associations: List<AssociationRepresentation>,
    val links: List<LinkRepresentation>,
    val references: List<ReferenceRepresentation>
)

sealed interface ConnectionRepresentation<T: ConnectionId> {
    val id: T
    @get:JsonProperty("target_building_block_instance")
    val targetBuildingBlockInstance: BuildingBlockInstanceId
    @get:JsonProperty("min_count")
    val minCount: Int
    @get:JsonProperty("max_count")
    val maxCount: Int
    @get:JsonProperty("created_by")
    val createdBy: ContributorId
    @get:JsonProperty("created_at")
    val createdAt: LocalDateTime
}

data class AssociationRepresentation(
    override val id: AssociationId,
    override val targetBuildingBlockInstance: BuildingBlockInstanceId,
    override val minCount: Int,
    override val maxCount: Int,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    val carryOverSubjectRangeConstraints: Set<ObjectPositionId>
) : ConnectionRepresentation<AssociationId>

data class LinkRepresentation(
    override val id: LinkId,
    override val targetBuildingBlockInstance: BuildingBlockInstanceId,
    override val minCount: Int,
    override val maxCount: Int,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    val useAsSubject: ObjectPositionId
) : ConnectionRepresentation<LinkId>

data class ReferenceRepresentation(
    override val id: ReferenceId,
    override val targetBuildingBlockInstance: BuildingBlockInstanceId,
    override val minCount: Int,
    override val maxCount: Int,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : ConnectionRepresentation<ReferenceId>
