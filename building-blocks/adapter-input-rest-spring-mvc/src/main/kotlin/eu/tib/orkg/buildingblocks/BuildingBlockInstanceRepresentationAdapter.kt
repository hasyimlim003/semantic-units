package eu.tib.orkg.buildingblocks

import eu.tib.orkg.buildingblocks.api.BuildingBlockInstanceRepresentation
import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstance
import org.springframework.data.domain.Page
import java.util.*

interface BuildingBlockInstanceRepresentationAdapter : ConnectionRepresentationAdapter {
    fun Optional<BuildingBlockInstance>.mapToBuildingBlockInstanceRepresentation(): Optional<BuildingBlockInstanceRepresentation> =
        map { it.toBuildingBlockInstanceRepresentation() }

    fun Page<BuildingBlockInstance>.mapToBuildingBlockInstanceRepresentation(): Page<BuildingBlockInstanceRepresentation> =
        map { it.toBuildingBlockInstanceRepresentation() }

    private fun BuildingBlockInstance.toBuildingBlockInstanceRepresentation(): BuildingBlockInstanceRepresentation =
        BuildingBlockInstanceRepresentation(
            id = id,
            buildingBlockId = buildingBlockId,
            createdBy = createdBy,
            createdAt = createdAt,
            associations = associations.values.map { it.toAssociationRepresentation() },
            links = links.values.map { it.toLinkRepresentation() },
            references = references.values.map { it.toReferenceRepresentation() }
        )
}
