package eu.tib.orkg.buildingblocks

import eu.tib.orkg.buildingblocks.api.ComparableConstraintRepresentation
import eu.tib.orkg.buildingblocks.api.ConstraintRepresentation
import eu.tib.orkg.buildingblocks.api.ResourceConstraintRepresentation
import eu.tib.orkg.buildingblocks.api.StringConstraintRepresentation
import eu.tib.orkg.buildingblocks.domain.*
import java.util.*

interface ConstraintRepresentationAdapter {
    fun Optional<Constraint>.mapToConstraintRepresentation(): Optional<ConstraintRepresentation> =
        map { it.toConstraintRepresentation() }

    fun Constraint.toConstraintRepresentation(): ConstraintRepresentation =
        when (this) {
            is ComparableConstraint<*> -> ComparableConstraintRepresentation(
                min = min,
                max = max,
                minExclusive = minExclusive,
                maxExclusive = maxExclusive
            )
            is ResourceConstraint -> ResourceConstraintRepresentation(ancestor)
            is StringConstraint -> StringConstraintRepresentation(pattern)
        }
}
