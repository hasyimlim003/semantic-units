package eu.tib.orkg.buildingblocks.application

import com.fasterxml.jackson.annotation.JsonProperty
import eu.tib.orkg.auth.api.UserUseCases
import eu.tib.orkg.buildingblocks.BuildingBlockRepresentationAdapter
import eu.tib.orkg.buildingblocks.api.BuildingBlockRepresentation
import eu.tib.orkg.buildingblocks.api.BuildingBlockUseCases
import eu.tib.orkg.buildingblocks.api.CreateBuildingBlockUseCase
import eu.tib.orkg.buildingblocks.domain.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.security.Principal
import java.util.*

@RestController
@RequestMapping("/api/building-blocks/classes", produces = [MediaType.APPLICATION_JSON_VALUE])
class BuildingBlockController(
    private val service: BuildingBlockUseCases,
    private val userService: UserUseCases
) : BuildingBlockRepresentationAdapter {
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun create(
        @RequestBody @Validated request: CreateBuildingBlockRequest,
        principal: Principal?,
        uriComponentsBuilder: UriComponentsBuilder
    ): ResponseEntity<BuildingBlockRepresentation> {
        val user = userService.authenticateUser(principal)
        val command = request.toCreateCommand(ContributorId(user.id))
        val id = service.create(command)
        val location = uriComponentsBuilder
            .path("api/building-blocks/classes/{id}") // TODO: check if path needs a leading '/'
            .buildAndExpand(id)
            .toUri()
        return created(location).body(service.findById(id).mapToBuildingBlockRepresentation().get())
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: BuildingBlockId): BuildingBlockRepresentation =
        service.findById(id)
            .mapToBuildingBlockRepresentation()
            .orElseThrow { BuildingBlockNotFound(id) }

    @GetMapping
    fun findAll(pageable: Pageable): Page<BuildingBlockRepresentation> =
        service.findAll(pageable).mapToBuildingBlockRepresentation()

    data class CreateBuildingBlockRequest(
        @NotBlank
        val label: String,
        @NotBlank
        val description: String,
        val type: BuildingBlock.Type,
        @JsonProperty("subject_constraint")
        val subjectConstraint: URI?,
        @NotEmpty
        @JsonProperty("object_position_classes")
        val objectPositionClasses: Set<ObjectPosition>
    ) {
        data class ObjectPosition(
            @JsonProperty("data_type")
            val dataType: DataType,
            @NotBlank
            val label: String,
            val required: Boolean,
            val constraint: Constraint?
        ) {
            fun toCreateCommand() : CreateBuildingBlockUseCase.CreateCommand.ObjectPosition =
                CreateBuildingBlockUseCase.CreateCommand.ObjectPosition(
                    type = dataType,
                    label = label,
                    required = required,
                    constraint = constraint
                )
        }

        fun toCreateCommand(contributorId: ContributorId): CreateBuildingBlockUseCase.CreateCommand =
            CreateBuildingBlockUseCase.CreateCommand(
                label = label,
                description = description,
                contributorId = contributorId,
                type = type,
                subjectConstraint = subjectConstraint,
                objectPositionClasses = objectPositionClasses.map { it.toCreateCommand() }.toSet()
            )
    }
}
