package eu.tib.orkg.buildingblocks

import eu.tib.orkg.buildingblocks.api.ObjectPositionRepresentation
import eu.tib.orkg.buildingblocks.domain.ObjectPosition
import java.util.*

interface ObjectPositionRepresentationAdapter : ConstraintRepresentationAdapter {
    fun Optional<ObjectPosition>.mapToObjectPositionRepresentation(): Optional<ObjectPositionRepresentation> =
        map { it.toObjectPositionRepresentation() }

    fun ObjectPosition.toObjectPositionRepresentation(): ObjectPositionRepresentation =
        ObjectPositionRepresentation(
            id = id,
            label = label,
            dataType = dataType,
            required = required,
            constraint = constraint?.toConstraintRepresentation(),
            createdBy = createdBy,
            createdAt = createdAt
        )
}
