package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryLinkRepository
import io.kotest.core.spec.style.DescribeSpec
import eu.tib.orkg.common.spi.idRepositoryContract

internal class InMemoryLinkAdapterContractTests : DescribeSpec({
    include(idRepositoryContract(InMemoryLinkAdapter(InMemoryLinkRepository())))
})
