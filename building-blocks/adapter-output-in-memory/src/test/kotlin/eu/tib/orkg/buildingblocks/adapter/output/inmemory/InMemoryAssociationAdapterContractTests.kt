package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryAssociationRepository
import io.kotest.core.spec.style.DescribeSpec
import eu.tib.orkg.common.spi.idRepositoryContract

internal class InMemoryAssociationAdapterContractTests : DescribeSpec({
    include(idRepositoryContract(InMemoryAssociationAdapter(InMemoryAssociationRepository())))
})
