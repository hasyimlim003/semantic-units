package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryReferenceRepository
import io.kotest.core.spec.style.DescribeSpec
import eu.tib.orkg.common.spi.idRepositoryContract

internal class InMemoryReferenceAdapterContractTests : DescribeSpec({
    include(idRepositoryContract(InMemoryReferenceAdapter(InMemoryReferenceRepository())))
})
