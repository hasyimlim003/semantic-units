package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryBuildingBlockRepository
import eu.tib.orkg.buildingblocks.spi.buildingBlockRepositoryContract
import eu.tib.orkg.common.spi.idRepositoryContract
import io.kotest.core.spec.style.DescribeSpec

internal class InMemoryBuildingBlockAdapterContractTests : DescribeSpec({
    // TODO: share instance?
    include(idRepositoryContract(InMemoryBuildingBlockAdapter(InMemoryBuildingBlockRepository())))
    include(buildingBlockRepositoryContract(InMemoryBuildingBlockAdapter(InMemoryBuildingBlockRepository())))
})
