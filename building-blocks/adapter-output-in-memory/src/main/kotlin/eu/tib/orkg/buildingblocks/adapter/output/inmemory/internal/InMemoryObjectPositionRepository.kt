package eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal

import eu.tib.orkg.buildingblocks.domain.ObjectPosition
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import eu.tib.orkg.common.adapter.output.inmemory.InMemoryRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryObjectPositionRepository : InMemoryRepository<ObjectPositionId, ObjectPosition>(
    idSelector = ObjectPosition::id,
    defaultComparator = compareBy(ObjectPosition::id)
) {
    override fun nextIdentity(): ObjectPositionId {
        var id = ObjectPositionId(UUID.randomUUID().toString())
        while(id in entities) {
            id = ObjectPositionId(UUID.randomUUID().toString())
        }
        return id
    }
}
