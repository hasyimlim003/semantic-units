package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryAssociationRepository
import eu.tib.orkg.buildingblocks.domain.AssociationId
import eu.tib.orkg.buildingblocks.spi.AssociationRepository
import org.springframework.stereotype.Component

@Component
class InMemoryAssociationAdapter(
    private val repository: InMemoryAssociationRepository
): AssociationRepository {
    override fun nextIdentity(): AssociationId = repository.nextIdentity()
}
