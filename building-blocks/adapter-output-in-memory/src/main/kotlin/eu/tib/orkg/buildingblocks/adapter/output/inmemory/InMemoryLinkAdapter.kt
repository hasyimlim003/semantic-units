package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryLinkRepository
import eu.tib.orkg.buildingblocks.domain.LinkId
import eu.tib.orkg.buildingblocks.spi.LinkRepository
import org.springframework.stereotype.Component

@Component
class InMemoryLinkAdapter(
    private val repository: InMemoryLinkRepository
): LinkRepository {
    override fun nextIdentity(): LinkId = repository.nextIdentity()
}
