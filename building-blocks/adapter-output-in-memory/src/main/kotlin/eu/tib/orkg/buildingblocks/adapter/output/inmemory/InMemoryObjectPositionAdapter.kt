package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryObjectPositionRepository
import eu.tib.orkg.buildingblocks.domain.ObjectPosition
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import eu.tib.orkg.buildingblocks.spi.ObjectPositionRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryObjectPositionAdapter(
    private val repository: InMemoryObjectPositionRepository
): ObjectPositionRepository {
    override fun save(entity: ObjectPosition) = repository.save(entity)

    override fun findAll(pageable: Pageable): Page<ObjectPosition> = repository.findAll(pageable)

    override fun findById(id: ObjectPositionId): Optional<ObjectPosition> = repository.findById(id)

    override fun deleteAll() = repository.deleteAll()

    override fun nextIdentity(): ObjectPositionId = repository.nextIdentity()
}
