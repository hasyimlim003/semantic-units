package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryReferenceRepository
import eu.tib.orkg.buildingblocks.domain.ReferenceId
import eu.tib.orkg.buildingblocks.spi.ReferenceRepository
import org.springframework.stereotype.Component

@Component
class InMemoryReferenceAdapter(
    private val repository: InMemoryReferenceRepository
): ReferenceRepository {
    override fun nextIdentity(): ReferenceId = repository.nextIdentity()
}
