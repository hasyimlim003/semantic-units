package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryBuildingBlockRepository
import eu.tib.orkg.buildingblocks.domain.BuildingBlock
import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.spi.BuildingBlockRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryBuildingBlockAdapter(
    private val repository: InMemoryBuildingBlockRepository
): BuildingBlockRepository {
    override fun save(entity: BuildingBlock) = repository.save(entity)

    override fun findAll(pageable: Pageable): Page<BuildingBlock> = repository.findAll(pageable)

    override fun findById(id: BuildingBlockId): Optional<BuildingBlock> = repository.findById(id)

    override fun deleteAll() = repository.deleteAll()

    override fun nextIdentity(): BuildingBlockId = repository.nextIdentity()
}
