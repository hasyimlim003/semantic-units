package eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal

import eu.tib.orkg.buildingblocks.domain.LinkId
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryLinkRepository {
    private val ids: Set<LinkId> = mutableSetOf()

    fun nextIdentity(): LinkId {
        var id = LinkId(UUID.randomUUID().toString())
        while(id in ids) {
            id = LinkId(UUID.randomUUID().toString())
        }
        return id
    }
}
