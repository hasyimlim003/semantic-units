package eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal

import eu.tib.orkg.buildingblocks.domain.BuildingBlock
import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.common.adapter.output.inmemory.InMemoryRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryBuildingBlockRepository : InMemoryRepository<BuildingBlockId, BuildingBlock>(
    idSelector = BuildingBlock::id,
    defaultComparator = compareBy(BuildingBlock::id)
) {
    override fun nextIdentity(): BuildingBlockId {
        var id = BuildingBlockId(UUID.randomUUID().toString())
        while(id in entities) {
            id = BuildingBlockId(UUID.randomUUID().toString())
        }
        return id
    }
}
