package eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal

import eu.tib.orkg.buildingblocks.domain.AssociationId
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryAssociationRepository {
    private val ids: Set<AssociationId> = mutableSetOf()

    fun nextIdentity(): AssociationId {
        var id = AssociationId(UUID.randomUUID().toString())
        while(id in ids) {
            id = AssociationId(UUID.randomUUID().toString())
        }
        return id
    }
}
