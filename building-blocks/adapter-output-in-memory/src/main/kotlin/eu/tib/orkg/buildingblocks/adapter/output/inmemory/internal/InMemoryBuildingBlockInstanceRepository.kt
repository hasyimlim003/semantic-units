package eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal

import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstance
import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import eu.tib.orkg.common.adapter.output.inmemory.InMemoryRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryBuildingBlockInstanceRepository : InMemoryRepository<BuildingBlockInstanceId, BuildingBlockInstance>(
    idSelector = BuildingBlockInstance::id,
    defaultComparator = compareBy(BuildingBlockInstance::id)
) {
    override fun nextIdentity(): BuildingBlockInstanceId {
        var id = BuildingBlockInstanceId(UUID.randomUUID().toString())
        while(id in entities) {
            id = BuildingBlockInstanceId(UUID.randomUUID().toString())
        }
        return id
    }
}
