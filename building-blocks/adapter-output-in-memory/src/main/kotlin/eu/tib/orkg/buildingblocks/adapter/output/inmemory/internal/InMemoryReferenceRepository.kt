package eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal

import eu.tib.orkg.buildingblocks.domain.ReferenceId
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryReferenceRepository {
    private val ids: Set<ReferenceId> = mutableSetOf()

    fun nextIdentity(): ReferenceId {
        var id = ReferenceId(UUID.randomUUID().toString())
        while(id in ids) {
            id = ReferenceId(UUID.randomUUID().toString())
        }
        return id
    }
}
