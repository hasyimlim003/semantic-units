package eu.tib.orkg.buildingblocks.adapter.output.inmemory

import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryBuildingBlockInstanceRepository
import eu.tib.orkg.buildingblocks.adapter.output.inmemory.internal.InMemoryBuildingBlockRepository
import eu.tib.orkg.buildingblocks.domain.BuildingBlock
import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstance
import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import eu.tib.orkg.buildingblocks.spi.BuildingBlockInstanceRepository
import eu.tib.orkg.buildingblocks.spi.BuildingBlockRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryBuildingBlockInstanceAdapter(
    private val repository: InMemoryBuildingBlockInstanceRepository
): BuildingBlockInstanceRepository {
    override fun save(entity: BuildingBlockInstance) = repository.save(entity)

    override fun findAll(pageable: Pageable): Page<BuildingBlockInstance> = repository.findAll(pageable)

    override fun findById(id: BuildingBlockInstanceId): Optional<BuildingBlockInstance> = repository.findById(id)

    override fun deleteAll() = repository.deleteAll()

    override fun nextIdentity(): BuildingBlockInstanceId = repository.nextIdentity()
}
