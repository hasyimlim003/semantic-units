package eu.tib.orkg.buildingblocks.services

import eu.tib.orkg.buildingblocks.application.ObjectPositionAlreadyExists
import eu.tib.orkg.buildingblocks.application.ObjectPositionService
import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.buildingblocks.dummyStringObjectPosition
import eu.tib.orkg.buildingblocks.spi.ObjectPositionRepository
import eu.tib.orkg.buildingblocks.toCreateCommand
import io.mockk.*
import org.junit.jupiter.api.assertThrows
import java.net.URI
import java.time.LocalDateTime
import java.util.*
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class ObjectPositionServiceUnitTest {
    private val repository: ObjectPositionRepository = mockk()
    private val staticClock: Clock = object : Clock {
        override fun now(): LocalDateTime = LocalDateTime.of(2023, 6, 14, 15, 13, 31, 12345)
    }
    private val service = ObjectPositionService(repository, staticClock)

    @BeforeTest
    fun resetState() {
        clearMocks(repository)
    }

    @AfterTest
    fun verifyMocks() {
        confirmVerified(repository)
    }

    @Test
    fun `Given an object position create command, when no id is specified, it gets a new id from the repository`() {
        val objectPosition = dummyStringObjectPosition()
        val command = objectPosition.toCreateCommand().copy(id = null)

        every { repository.nextIdentity() } returns objectPosition.id
        every { repository.save(any()) } just runs

        service.create(command)

        verify(exactly = 1) { repository.nextIdentity() }
        verify(exactly = 1) { repository.save(withArg { assertEquals(objectPosition.id, it.id) }) }
    }

    @Test
    fun `Given an object position create command, when an id is specified id but an object position for it does not already exist, then a new object position is created`() {
        val objectPosition = dummyStringObjectPosition()
        val command = objectPosition.toCreateCommand()

        every { repository.findById(command.id!!) } returns Optional.empty()
        every { repository.save(any()) } just runs

        service.create(command)

        verify(exactly = 0) { repository.nextIdentity() }
        verify(exactly = 1) { repository.findById(command.id!!) }
        verify(exactly = 1) { repository.save(withArg { assertEquals(objectPosition.id, it.id) }) }
    }

    @Test
    fun `Given an object position create command, when the specified id already exists, an exception is thrown`() {
        val objectPosition = dummyStringObjectPosition()
        val command = objectPosition.toCreateCommand()

        every { repository.findById(objectPosition.id) } returns Optional.of(objectPosition)

        assertThrows<ObjectPositionAlreadyExists> {
            service.create(command)
        }

        verify(exactly = 1) { repository.findById(objectPosition.id) }
        verify(exactly = 0) { repository.save(any()) }
    }

    @Test
    fun `Given an object position create command, when the given data type does not match the given constraint, an exception is thrown`() {
        val constraints: Map<DataType, Constraint?> = mapOf(
            DataType.INTEGER to IntegerConstraint(min = null, max = null, minExclusive = false, maxExclusive = false),
            DataType.DECIMAL to DecimalConstraint(min = null, max = null, minExclusive = false, maxExclusive = false),
            DataType.DATE_TIME to DateTimeConstraint(
                min = null,
                max = null,
                minExclusive = false,
                maxExclusive = false
            ),
            DataType.STRING to StringConstraint(pattern = "abc"),
            DataType.RESOURCE to ResourceConstraint(ancestor = URI.create("https://example.org")),
            DataType.BOOLEAN to null
        )
        val objectPosition = dummyStringObjectPosition()

        DataType.values().forEach { dataType ->
            constraints.entries.forEach { (associatedDataType, constraint) ->
                val command = objectPosition.toCreateCommand().copy(
                    id = null,
                    type = dataType,
                    constraint = constraint
                )

                // we have to check for constraint != null here because boolean does not accept any constraints
                if (dataType != associatedDataType && constraint != null) {
                    resetState()

                    every { repository.nextIdentity() } returns objectPosition.id

                    assertThrows<RuntimeException> {
                        service.create(command)
                    }

                    verify(exactly = 1) { repository.nextIdentity() }
                    verify(exactly = 0) { repository.save(any()) }

                    verifyMocks()
                }
            }
        }
    }

    @Test
    fun `Given an object position create command, when the validation succeeds, an object position is created`() {
        val constraints: Map<DataType, Constraint?> = mapOf(
            DataType.INTEGER to IntegerConstraint(min = null, max = null, minExclusive = false, maxExclusive = false),
            DataType.DECIMAL to DecimalConstraint(min = null, max = null, minExclusive = false, maxExclusive = false),
            DataType.DATE_TIME to DateTimeConstraint(
                min = null,
                max = null,
                minExclusive = false,
                maxExclusive = false
            ),
            DataType.STRING to StringConstraint(pattern = "abc"),
            DataType.RESOURCE to ResourceConstraint(ancestor = URI.create("https://example.org")),
            DataType.BOOLEAN to null
        )
        val objectPosition = dummyStringObjectPosition()

        constraints.entries.forEach { (dataType, constraint) ->
            val command = objectPosition.toCreateCommand().copy(
                id = null,
                type = dataType,
                constraint = constraint
            )

            resetState()

            every { repository.nextIdentity() } returns objectPosition.id
            every { repository.save(any()) } just runs

            service.create(command)

            verify(exactly = 1) { repository.nextIdentity() }
            verify(exactly = 1) { repository.save(withArg { assertEquals(constraint, it.constraint) }) }

            verifyMocks()
        }
    }
}
