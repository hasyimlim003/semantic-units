package eu.tib.orkg.buildingblocks.services

import eu.tib.orkg.buildingblocks.api.ObjectPositionUseCases
import eu.tib.orkg.buildingblocks.application.BuildingBlockAlreadyExists
import eu.tib.orkg.buildingblocks.application.BuildingBlockService
import eu.tib.orkg.buildingblocks.domain.Clock
import eu.tib.orkg.buildingblocks.dummyBuildingBlock
import eu.tib.orkg.buildingblocks.dummyStringObjectPosition
import eu.tib.orkg.buildingblocks.spi.BuildingBlockRepository
import eu.tib.orkg.buildingblocks.toCreateCommand
import io.mockk.*
import org.junit.jupiter.api.assertThrows
import java.time.LocalDateTime
import java.util.*
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class BuildingBlockServiceUnitTest {
    private val repository: BuildingBlockRepository = mockk()
    private val objectPositionService: ObjectPositionUseCases = mockk()
    private val staticClock: Clock = object : Clock {
        override fun now(): LocalDateTime = LocalDateTime.of(2023, 6, 21, 14, 38, 22, 12345)
    }
    private val service = BuildingBlockService(repository, objectPositionService, staticClock)

    @BeforeTest
    fun resetState() {
        clearMocks(repository)
    }

    @AfterTest
    fun verifyMocks() {
        confirmVerified(repository)
    }

    @Test
    fun `Given a building block create command, when no id is specified, it gets a new id from the repository`() {
        val objectPosition = dummyStringObjectPosition()
        val buildingBlock = dummyBuildingBlock(objectPositionClasses = setOf(objectPosition))
        val command = buildingBlock.toCreateCommand().copy(id = null)

        every { repository.nextIdentity() } returns buildingBlock.id
        every { repository.save(any()) } just runs
        every { objectPositionService.create(any()) } returns objectPosition.id
        every { objectPositionService.findById(objectPosition.id) } returns Optional.of(objectPosition)

        service.create(command)

        verify(exactly = 1) { repository.nextIdentity() }
        verify(exactly = 1) { repository.save(withArg { assertEquals(buildingBlock.id, it.id) }) }
        verify(exactly = 1) { objectPositionService.create(withArg { assertEquals(null, it.id) }) }
        verify(exactly = 1) { objectPositionService.findById(objectPosition.id) }
    }

    @Test
    fun `Given a building block create command, when an id is specified id but a building block for it does not already exist, then a new building block is created`() {
        val objectPosition = dummyStringObjectPosition()
        val buildingBlock = dummyBuildingBlock(objectPositionClasses = setOf(objectPosition))
        val command = buildingBlock.toCreateCommand()

        every { repository.findById(command.id!!) } returns Optional.empty()
        every { repository.save(any()) } just runs
        every { objectPositionService.create(any()) } returns objectPosition.id
        every { objectPositionService.findById(objectPosition.id) } returns Optional.of(objectPosition)

        service.create(command)

        verify(exactly = 0) { repository.nextIdentity() }
        verify(exactly = 1) { repository.findById(command.id!!) }
        verify(exactly = 1) { repository.save(withArg { assertEquals(buildingBlock.id, it.id) }) }
        verify(exactly = 1) { objectPositionService.create(withArg { assertEquals(null, it.id) }) }
        verify(exactly = 1) { objectPositionService.findById(objectPosition.id) }
    }

    @Test
    fun `Given a building block create command, when the specified id already exists, an exception is thrown`() {
        val buildingBlock = dummyBuildingBlock()
        val command = buildingBlock.toCreateCommand()

        every { repository.findById(command.id!!) } returns Optional.of(buildingBlock)

        assertThrows<BuildingBlockAlreadyExists> {
            service.create(command)
        }

        verify(exactly = 1) { repository.findById(command.id!!) }
        verify(exactly = 0) { repository.nextIdentity() }
        verify(exactly = 0) { repository.save(any()) }
    }
}
