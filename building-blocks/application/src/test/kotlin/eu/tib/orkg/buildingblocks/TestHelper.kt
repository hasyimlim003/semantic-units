package eu.tib.orkg.buildingblocks

import eu.tib.orkg.buildingblocks.api.CreateBuildingBlockUseCase
import eu.tib.orkg.buildingblocks.api.CreateObjectPositionUseCase
import eu.tib.orkg.buildingblocks.domain.*
import java.net.URI
import java.time.LocalDateTime
import java.util.*

fun dummyStringObjectPosition(
    id: ObjectPositionId = ObjectPositionId(UUID.randomUUID().toString()),
    label: String = "string",
    required: Boolean = false,
    constraint: StringConstraint? = null,
    createdBy: ContributorId = ContributorId.UNKNOWN,
    createdAt: LocalDateTime = LocalDateTime.now()
) = StringObjectPosition(
    id = id,
    label = label,
    required = required,
    constraint = constraint,
    createdBy = createdBy,
    createdAt = createdAt
)

fun dummyBuildingBlock(
    id: BuildingBlockId = BuildingBlockId(UUID.randomUUID().toString()),
    label: String = "building block",
    description: String = "description",
    type: BuildingBlock.Type = BuildingBlock.Type.STATEMENT,
    createdBy: ContributorId = ContributorId.UNKNOWN,
    createdAt: LocalDateTime = LocalDateTime.now(),
    subjectConstraint: URI? = null,
    objectPositionClasses: Set<ObjectPosition> = emptySet()
) = BuildingBlock(
    id = id,
    label = label,
    description = description,
    type = type,
    createdBy = createdBy,
    createdAt = createdAt,
    subjectConstraint = subjectConstraint,
    objectPositionClasses = objectPositionClasses.associateBy { it.id }
)

fun ObjectPosition.toCreateCommand(): CreateObjectPositionUseCase.CreateCommand =
    CreateObjectPositionUseCase.CreateCommand(
        id = this@toCreateCommand.id,
        type = this@toCreateCommand.dataType,
        label = this@toCreateCommand.label,
        required = this@toCreateCommand.required,
        contributorId = this@toCreateCommand.createdBy,
        constraint = this@toCreateCommand.constraint
    )

fun ObjectPosition.toBuildingBlockCreateCommand(): CreateBuildingBlockUseCase.CreateCommand.ObjectPosition =
    CreateBuildingBlockUseCase.CreateCommand.ObjectPosition(
        type = this@toBuildingBlockCreateCommand.dataType,
        label = this@toBuildingBlockCreateCommand.label,
        required = this@toBuildingBlockCreateCommand.required,
        constraint = this@toBuildingBlockCreateCommand.constraint
    )

fun BuildingBlock.toCreateCommand():CreateBuildingBlockUseCase.CreateCommand =
    CreateBuildingBlockUseCase.CreateCommand(
        id = this@toCreateCommand.id,
        label = this@toCreateCommand.label,
        description = this@toCreateCommand.description,
        type = this@toCreateCommand.type,
        contributorId = this@toCreateCommand.createdBy,
        subjectConstraint = this@toCreateCommand.subjectConstraint,
        objectPositionClasses = this@toCreateCommand.objectPositionClasses.values
            .map { it.toBuildingBlockCreateCommand() }
            .toSet()
    )
