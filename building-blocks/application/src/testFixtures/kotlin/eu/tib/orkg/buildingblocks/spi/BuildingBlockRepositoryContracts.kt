package eu.tib.orkg.buildingblocks.spi

import dev.forkhandles.fabrikate.FabricatorConfig
import dev.forkhandles.fabrikate.Fabrikate
import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.common.application.PageRequests
import io.kotest.assertions.asClue
import io.kotest.core.spec.style.describeSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.comparables.shouldBeLessThan
import io.kotest.matchers.maps.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.springframework.data.domain.PageRequest
import java.net.URI
import java.time.LocalDateTime
import java.util.*

fun buildingBlockRepositoryContract(
    repository: BuildingBlockRepository
) = describeSpec {
    beforeTest {
        repository.deleteAll()
    }

    val fabricator = Fabrikate(
        FabricatorConfig(
            collectionSizes = 12..12,
            nullableStrategy = FabricatorConfig.NullableStrategy.NeverSetToNull
        ).withStandardMappings()
            .withBuildingBlocks()
    )

    describe("saving a building block") {
        val buildingBlock = BuildingBlock(
            id = BuildingBlockId("BlockId"),
            label = "building block",
            description = "description",
            type = BuildingBlock.Type.STATEMENT,
            createdBy = ContributorId.random(),
            createdAt = LocalDateTime.now(),
            subjectConstraint = URI.create("https://example.org"),
            objectPositionClasses = listOf(
                StringObjectPosition(
                    id = ObjectPositionId(UUID.randomUUID().toString()),
                    label = "string",
                    required = false,
                    constraint = null,
                    createdBy = ContributorId.UNKNOWN,
                    createdAt = LocalDateTime.now()
                ),
                IntegerObjectPosition(
                    id = ObjectPositionId(UUID.randomUUID().toString()),
                    label = "integer",
                    required = true,
                    constraint = IntegerConstraint(0, 10, minExclusive = false, maxExclusive = false),
                    createdBy = ContributorId.UNKNOWN,
                    createdAt = LocalDateTime.now()
                )
            ).associateBy { it.id }
        )
        it("saves and loads all properties correctly") {
            repository.save(buildingBlock)

            val actual = repository.findById(buildingBlock.id)

            actual.isPresent shouldBe true
            actual.get() shouldNotBe null
            actual.get().asClue {
                it.id shouldBe buildingBlock.id
                it.label shouldBe buildingBlock.label
                it.description shouldBe buildingBlock.description
                it.type shouldBe buildingBlock.type
                it.createdBy shouldBe buildingBlock.createdBy
                it.createdAt shouldBe buildingBlock.createdAt
                it.subjectConstraint shouldBe buildingBlock.subjectConstraint
                it.objectPositionClasses shouldContainExactly buildingBlock.objectPositionClasses
            }
        }
        it("updates an already existing building block") {
            repository.save(buildingBlock)
            val modified = buildingBlock.copy(
                label = "modified building block",
                description = "modified description",
                subjectConstraint = URI.create("https://orkg.org"),
                objectPositionClasses = listOf(
                    StringObjectPosition(
                        id = ObjectPositionId(UUID.randomUUID().toString()),
                        label = "string",
                        required = false,
                        constraint = null,
                        createdBy = ContributorId.UNKNOWN,
                        createdAt = LocalDateTime.now()
                    ),
                    IntegerObjectPosition(
                        id = ObjectPositionId(UUID.randomUUID().toString()),
                        label = "integer",
                        required = true,
                        constraint = IntegerConstraint(0, 10, minExclusive = false, maxExclusive = false),
                        createdBy = ContributorId.UNKNOWN,
                        createdAt = LocalDateTime.now()
                    )
                ).associateBy { it.id }
            )
            repository.save(buildingBlock)
            repository.findById(buildingBlock.id).isPresent shouldNotBe false
            repository.save(modified)

            val actual = repository.findById(buildingBlock.id)

            repository.findAll(PageRequests.ALL).toSet().size shouldBe 1
            actual.isPresent shouldBe true
            actual.get() shouldNotBe null
            actual.get().asClue {
                it.id shouldBe modified.id
                it.label shouldBe modified.label
                it.description shouldBe modified.description
                it.type shouldBe modified.type
                it.createdBy shouldBe modified.createdBy
                it.createdAt shouldBe modified.createdAt
                it.subjectConstraint shouldBe modified.subjectConstraint
                it.objectPositionClasses shouldContainExactly modified.objectPositionClasses
            }
        }
    }
    context("finding several building blocks") {
        val buildingBlocks = fabricator.random<List<BuildingBlock>>()
        buildingBlocks.forEach(repository::save)

        val expected = buildingBlocks.sortedBy { it.id }.drop(5).take(5)

        // Explicitly requesting second page here
        val result = repository.findAll(PageRequest.of(1, 5))

        it("returns the correct result") {
            result shouldNotBe null
            result.content shouldNotBe null
            result.content.size shouldBe expected.size
            result.content shouldContainExactly expected
        }
        it("pages the results correctly") {
            result.size shouldBe 5
            result.number shouldBe 1 // 0-indexed
            result.totalPages shouldBe 3
            result.totalElements shouldBe 12
        }
        it("sorts the results by id by default") {
            result.content.zipWithNext { a, b ->
                a.id shouldBeLessThan b.id
            }
        }
    }
    context("deleting all building blocks") {
        val buildingBlocks = fabricator.random<List<BuildingBlock>>()
        buildingBlocks.forEach(repository::save)
        repository.findAll(PageRequests.ALL).totalElements shouldBe buildingBlocks.size
        repository.deleteAll()
        repository.findAll(PageRequests.ALL).totalElements shouldBe 0
    }
}
