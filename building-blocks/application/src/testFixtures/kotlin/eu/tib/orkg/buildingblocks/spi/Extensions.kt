package eu.tib.orkg.buildingblocks.spi

import dev.forkhandles.fabrikate.FabricatorConfig
import dev.forkhandles.fabrikate.Fabrikate
import dev.forkhandles.fabrikate.UUIDFabricator
import dev.forkhandles.fabrikate.register
import eu.tib.orkg.buildingblocks.domain.*
import java.time.LocalDateTime

fun FabricatorConfig.withBuildingBlockIds() = withMapping {
    BuildingBlockId(UUIDFabricator()(it).toString())
}

fun FabricatorConfig.withObjectPositionIds() = withMapping {
    ObjectPositionId(UUIDFabricator()(it).toString())
}

fun FabricatorConfig.withContributorIds() = withMapping {
    ContributorId(UUIDFabricator()(it).toString())
}

fun FabricatorConfig.withObjectPositions() = withMappings {
    register {
        IntegerConstraint(
            min = it.config.random.nextLong(-100, 0).randomToNull(it),
            max = it.config.random.nextLong(0, 100).randomToNull(it),
            minExclusive = it.random(),
            maxExclusive = it.random()
        )
    }
    register {
        DecimalConstraint(
            min = it.config.random.nextDouble(-100.0, 0.0).randomToNull(it),
            max = it.config.random.nextDouble(0.0, 100.0).randomToNull(it),
            minExclusive = it.random(),
            maxExclusive = it.random()
        )
    }
    register {
        StringConstraint(
            pattern = when (it.config.random.nextInt(4)) {
                0 -> """\w+"""
                1 -> """\d+"""
                2 -> """\w"""
                3 -> """\d"""
                else -> throw IndexOutOfBoundsException()
            }
        )
    }
    register {
        ResourceConstraint(it.random())
    }
    register {
        val dateTime = it.random<LocalDateTime>()
        DateTimeConstraint(
            min = dateTime.randomToNull(it),
            max = dateTime.randomToNull(it)?.plusNanos(it.config.random.nextLong(1_000_000_000_000_000)),
            minExclusive = it.random(),
            maxExclusive = it.random()
        )
    }
    register<ObjectPosition> {
        when (it.config.random.nextInt(6)) {
            0 -> IntegerObjectPosition(
                id = it.random(),
                label = it.random(),
                required = it.random(),
                constraint = it.random<IntegerConstraint>().randomToNull(it),
                createdBy = it.random(),
                createdAt = it.random()
            )
            1 -> DecimalObjectPosition(
                id = it.random(),
                label = it.random(),
                required = it.random(),
                constraint = it.random<DecimalConstraint>().randomToNull(it),
                createdBy = it.random(),
                createdAt = it.random()
            )
            2 -> StringObjectPosition(
                id = it.random(),
                label = it.random(),
                required = it.random(),
                constraint = it.random<StringConstraint>().randomToNull(it),
                createdBy = it.random(),
                createdAt = it.random()
            )
            3 -> ResourceObjectPosition(
                id = it.random(),
                label = it.random(),
                required = it.random(),
                constraint = it.random<ResourceConstraint>().randomToNull(it),
                createdBy = it.random(),
                createdAt = it.random()
            )
            4 -> BooleanObjectPosition(
                id = it.random(),
                label = it.random(),
                required = it.random(),
                createdBy = it.random(),
                createdAt = it.random()
            )
            5 -> DateTimeObjectPosition(
                id = it.random(),
                label = it.random(),
                required = it.random(),
                constraint = it.random<DateTimeConstraint>().randomToNull(it),
                createdBy = it.random(),
                createdAt = it.random()
            )
            else -> throw IndexOutOfBoundsException()
        }
    }
}.withObjectPositionIds()

fun FabricatorConfig.withBuildingBlocks() = withMapping {
    BuildingBlock(
        id = it.random(),
        label = it.random(),
        description = it.random(),
        type = it.random(),
        createdBy = it.random(),
        createdAt = it.random(),
        subjectConstraint = it.random(),
        objectPositionClasses = it.random<List<ObjectPosition>>().associateBy(ObjectPosition::id)
    )
}.withBuildingBlockIds()
    .withContributorIds()
    .withObjectPositions()

private fun <T> T.randomToNull(fabrikate: Fabrikate): T? = takeIf { fabrikate.random() }
