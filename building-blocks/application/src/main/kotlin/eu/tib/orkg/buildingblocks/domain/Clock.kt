package eu.tib.orkg.buildingblocks.domain

import org.springframework.stereotype.Component
import java.time.LocalDateTime

interface Clock {
    fun now(): LocalDateTime
}

@Component
class SystemClock : Clock {
    override fun now(): LocalDateTime = LocalDateTime.now()
}
