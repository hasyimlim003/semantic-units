package eu.tib.orkg.buildingblocks.api

import eu.tib.orkg.buildingblocks.domain.*

interface CreateBuildingBlockInstanceUseCase {
    fun create(command: CreateCommand): BuildingBlockInstanceId

    data class CreateCommand(
        val id: BuildingBlockInstanceId? = null,
        val buildingBlockId: BuildingBlockId,
        val contributorId: ContributorId,
        val associations: Set<Association>,
        val links: Set<Link>,
        val references: Set<Reference>
    ) {
        data class Association(
            val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
            val minCount: Int,
            val maxCount: Int,
            val carryOverSubjectRangeConstraints: Set<ObjectPositionId>
        )

        data class Link(
            val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
            val minCount: Int,
            val maxCount: Int,
            val useAsSubject: ObjectPositionId
        )

        data class Reference(
            val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
            val minCount: Int,
            val maxCount: Int,
        )
    }
}

interface CreateAssociationUseCase {
    fun createAssociation(command: CreateAssociationCommand): AssociationId

    data class CreateAssociationCommand(
        val contributorId: ContributorId,
        val sourceBuildingBlockInstanceId: BuildingBlockInstanceId,
        val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        val minCount: Int,
        val maxCount: Int,
        val carryOverSubjectRangeConstraints: Set<ObjectPositionId>
    )
}

interface CreateLinkUseCase {
    fun createLink(command: CreateLinkCommand): LinkId

    data class CreateLinkCommand(
        val contributorId: ContributorId,
        val sourceBuildingBlockInstanceId: BuildingBlockInstanceId,
        val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        val minCount: Int,
        val maxCount: Int,
        val useAsSubject: ObjectPositionId
    )
}

interface CreateReferenceUseCase {
    fun createReference(command: CreateReferenceCommand): ReferenceId

    data class CreateReferenceCommand(
        val contributorId: ContributorId,
        val sourceBuildingBlockInstanceId: BuildingBlockInstanceId,
        val targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        val minCount: Int,
        val maxCount: Int
    )
}

interface DeleteBuildingBlockInstanceUseCase {
    fun deleteAll()
}
