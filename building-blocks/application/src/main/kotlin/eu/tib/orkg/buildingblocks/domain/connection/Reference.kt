package eu.tib.orkg.buildingblocks.domain.connection

import eu.tib.orkg.buildingblocks.domain.*
import java.time.LocalDateTime

data class Reference(
    override val id: ReferenceId,
    override val targetBuildingBlockInstance: BuildingBlockInstanceId,
    override val minCount: Int,
    override val maxCount: Int,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : Connection<ReferenceId>
