package eu.tib.orkg.buildingblocks.api

import eu.tib.orkg.buildingblocks.domain.BuildingBlock
import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.util.Optional

interface RetrieveBuildingBlockUseCase {
    fun findById(buildingBlockId: BuildingBlockId): Optional<BuildingBlock>
    fun findAll(pageable: Pageable): Page<BuildingBlock>
}
