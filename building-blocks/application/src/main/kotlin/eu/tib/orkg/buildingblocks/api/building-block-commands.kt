package eu.tib.orkg.buildingblocks.api

import eu.tib.orkg.buildingblocks.domain.*
import java.net.URI

interface CreateBuildingBlockUseCase {
    fun create(command: CreateCommand): BuildingBlockId

    data class CreateCommand(
        val id: BuildingBlockId? = null,
        val label: String,
        val description: String,
        val type: BuildingBlock.Type,
        val contributorId: ContributorId,
        val subjectConstraint: URI?,
        val objectPositionClasses: Set<ObjectPosition>
    ) {
        data class ObjectPosition(
            val type: DataType,
            val label: String,
            val required: Boolean,
            val constraint: Constraint?
        )
    }
}

interface DeleteBuildingBlockUseCase {
    fun deleteAll()
}
