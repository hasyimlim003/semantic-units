package eu.tib.orkg.buildingblocks.spi

import eu.tib.orkg.buildingblocks.domain.LinkId
import eu.tib.orkg.common.spi.IdRepository

interface LinkRepository : IdRepository<LinkId>
