package eu.tib.orkg.buildingblocks.spi

import eu.tib.orkg.buildingblocks.domain.ReferenceId
import eu.tib.orkg.common.spi.IdRepository

interface ReferenceRepository : IdRepository<ReferenceId>
