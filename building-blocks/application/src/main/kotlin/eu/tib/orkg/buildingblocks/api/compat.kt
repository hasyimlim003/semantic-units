package eu.tib.orkg.buildingblocks.api

interface BuildingBlockUseCases :
    CreateBuildingBlockUseCase,
    RetrieveBuildingBlockUseCase,
    DeleteBuildingBlockUseCase

interface BuildingBlockInstanceUseCases :
    CreateBuildingBlockInstanceUseCase,
    CreateAssociationUseCase,
    CreateLinkUseCase,
    CreateReferenceUseCase,
    RetrieveBuildingBlockInstanceUseCase,
    DeleteBuildingBlockInstanceUseCase

interface ObjectPositionUseCases :
    CreateObjectPositionUseCase,
    RetrieveObjectPositionUseCase,
    DeleteObjectPositionUseCase
