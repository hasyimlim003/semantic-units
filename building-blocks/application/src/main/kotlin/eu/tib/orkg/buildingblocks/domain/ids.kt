package eu.tib.orkg.buildingblocks.domain

import eu.tib.orkg.common.domain.Id
import java.util.*

sealed interface ConnectionId

class LinkId(value: String) : Id<LinkId>(value), ConnectionId
class AssociationId(value: String) : Id<AssociationId>(value), ConnectionId
class ReferenceId(value: String) : Id<ReferenceId>(value), ConnectionId
class ObjectPositionId(value: String) : Id<ObjectPositionId>(value)
class BuildingBlockId(value: String) : Id<BuildingBlockId>(value)
class BuildingBlockInstanceId(value: String) : Id<BuildingBlockInstanceId>(value)

data class ContributorId(val value: UUID) {

    constructor(s: String) : this(UUID.fromString(s))

    companion object {
        val UNKNOWN: ContributorId = ContributorId(UUID(0, 0))
        val SYSTEM: ContributorId = ContributorId(UUID(-1, -1))
        fun random() = ContributorId(UUID.randomUUID())
    }
}
