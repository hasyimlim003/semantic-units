package eu.tib.orkg.buildingblocks.spi

import eu.tib.orkg.buildingblocks.domain.ObjectPosition
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import eu.tib.orkg.common.spi.EntityRepository

interface ObjectPositionRepository : EntityRepository<ObjectPositionId, ObjectPosition>
