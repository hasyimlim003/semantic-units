package eu.tib.orkg.buildingblocks.domain

enum class DataType {
    STRING,
    INTEGER,
    DECIMAL,
    RESOURCE,
    BOOLEAN,
    DATE_TIME
}
