package eu.tib.orkg.buildingblocks.application

import eu.tib.orkg.buildingblocks.api.CreateObjectPositionUseCase
import eu.tib.orkg.buildingblocks.api.ObjectPositionUseCases
import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.buildingblocks.spi.ObjectPositionRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*

@Service
class ObjectPositionService(
    private val repository: ObjectPositionRepository,
    private val clock: Clock = SystemClock()
) : ObjectPositionUseCases {
    override fun create(command: CreateObjectPositionUseCase.CreateCommand): ObjectPositionId {
        val id = command.id?.apply {
            repository.findById(this).ifPresent { throw ObjectPositionAlreadyExists(it.id) }
        } ?: repository.nextIdentity()
        val objectPosition = when (command.type) {
            DataType.STRING -> StringObjectPosition(
                id = id,
                label = command.label,
                required = command.required,
                constraint = command.constraint.assertType<StringConstraint?>(DataType.STRING),
                createdBy = command.contributorId,
                createdAt = clock.now()
            )
            DataType.INTEGER -> IntegerObjectPosition(
                id = id,
                label = command.label,
                required = command.required,
                constraint = command.constraint.assertType<IntegerConstraint?>(DataType.INTEGER),
                createdBy = command.contributorId,
                createdAt = clock.now()
            )
            DataType.DECIMAL -> DecimalObjectPosition(
                id = id,
                label = command.label,
                required = command.required,
                constraint = command.constraint.assertType<DecimalConstraint?>(DataType.DECIMAL),
                createdBy = command.contributorId,
                createdAt = clock.now()
            )
            DataType.RESOURCE -> ResourceObjectPosition(
                id = id,
                label = command.label,
                required = command.required,
                constraint = command.constraint.assertType<ResourceConstraint?>(DataType.RESOURCE),
                createdBy = command.contributorId,
                createdAt = clock.now()
            )
            DataType.BOOLEAN -> {
                if (command.constraint != null) {
                    throw RuntimeException("Constraints on booleans are not allowed.")
                }
                BooleanObjectPosition(
                    id = id,
                    label = command.label,
                    required = command.required,
                    createdBy = command.contributorId,
                    createdAt = clock.now()
                )
            }
            DataType.DATE_TIME -> DateTimeObjectPosition(
                id = id,
                label = command.label,
                required = command.required,
                constraint = command.constraint.assertType<DateTimeConstraint?>(DataType.DATE_TIME),
                createdBy = command.contributorId,
                createdAt = clock.now()
            )
        }
        repository.save(objectPosition)
        return objectPosition.id
    }

    override fun findById(objectPositionId: ObjectPositionId): Optional<ObjectPosition> =
        repository.findById(objectPositionId)

    override fun findAll(pageable: Pageable): Page<ObjectPosition> =
        repository.findAll(pageable)

    override fun deleteAll() = repository.deleteAll()

    private inline fun <reified T : Constraint?> Any?.assertType(dataType: DataType): T? =
        when (this) {
            is T -> this
            else -> throw RuntimeException("""Constraint is not applicable for data type "$dataType".""")
        }
}
