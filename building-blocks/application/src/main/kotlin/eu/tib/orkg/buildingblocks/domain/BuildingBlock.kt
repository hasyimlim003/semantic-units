package eu.tib.orkg.buildingblocks.domain

import java.net.URI
import java.time.LocalDateTime

/**
 * Definition of a Knowledge Graph Building Block
 */
data class BuildingBlock(
    val id: BuildingBlockId,
    val label: String,
    val description: String,
    val type: Type,
    val createdBy: ContributorId,
    val createdAt: LocalDateTime,
    val subjectConstraint: URI?,
    val objectPositionClasses: Map<ObjectPositionId, ObjectPosition>
) {
    enum class Type {
        STATEMENT
    }
}
