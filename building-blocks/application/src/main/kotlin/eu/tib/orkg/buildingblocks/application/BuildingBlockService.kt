package eu.tib.orkg.buildingblocks.application

import eu.tib.orkg.buildingblocks.api.BuildingBlockUseCases
import eu.tib.orkg.buildingblocks.api.CreateBuildingBlockUseCase
import eu.tib.orkg.buildingblocks.api.CreateObjectPositionUseCase
import eu.tib.orkg.buildingblocks.api.ObjectPositionUseCases
import eu.tib.orkg.buildingblocks.domain.BuildingBlock
import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.Clock
import eu.tib.orkg.buildingblocks.domain.SystemClock
import eu.tib.orkg.buildingblocks.spi.BuildingBlockRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*

@Service
class BuildingBlockService(
    private val repository: BuildingBlockRepository,
    private val objectPositionService: ObjectPositionUseCases,
    private val clock: Clock = SystemClock()
) : BuildingBlockUseCases {
    override fun create(command: CreateBuildingBlockUseCase.CreateCommand): BuildingBlockId {
        val id = command.id?.apply {
            repository.findById(this).ifPresent { throw BuildingBlockAlreadyExists(it.id) }
        } ?: repository.nextIdentity()
        val buildingBlock = BuildingBlock(
            id = id,
            label = command.label,
            description = command.description,
            type = command.type,
            createdBy = command.contributorId,
            createdAt = clock.now(),
            subjectConstraint = command.subjectConstraint,
            objectPositionClasses = command.objectPositionClasses.associate {
                val objectPositionId = objectPositionService.create(
                    CreateObjectPositionUseCase.CreateCommand(
                        type = it.type,
                        label = it.label,
                        required = it.required,
                        contributorId = command.contributorId,
                        constraint = it.constraint
                    )
                )
                val objectPosition = objectPositionService.findById(objectPositionId).get()
                objectPositionId to objectPosition
            }
        )
        repository.save(buildingBlock)
        return buildingBlock.id
    }

    override fun findById(buildingBlockId: BuildingBlockId): Optional<BuildingBlock> =
        repository.findById(buildingBlockId)

    override fun findAll(pageable: Pageable): Page<BuildingBlock> =
        repository.findAll(pageable)

    override fun deleteAll() = repository.deleteAll()
}
