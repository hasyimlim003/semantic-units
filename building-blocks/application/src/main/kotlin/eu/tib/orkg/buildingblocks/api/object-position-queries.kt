package eu.tib.orkg.buildingblocks.api

import eu.tib.orkg.buildingblocks.domain.ObjectPosition
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.util.*

interface RetrieveObjectPositionUseCase {
    fun findById(objectPositionId: ObjectPositionId): Optional<ObjectPosition>
    fun findAll(pageable: Pageable): Page<ObjectPosition>
}
