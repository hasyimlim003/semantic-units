package eu.tib.orkg.buildingblocks.domain.connection

import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.buildingblocks.domain.LinkId
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import java.time.LocalDateTime

data class Link(
    override val id: LinkId,
    override val targetBuildingBlockInstance: BuildingBlockInstanceId,
    override val minCount: Int,
    override val maxCount: Int,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    val useAsSubject: ObjectPositionId
) : Connection<LinkId>
