package eu.tib.orkg.buildingblocks.domain

import eu.tib.orkg.buildingblocks.domain.connection.Association
import eu.tib.orkg.buildingblocks.domain.connection.Connection
import eu.tib.orkg.buildingblocks.domain.connection.Link
import eu.tib.orkg.buildingblocks.domain.connection.Reference
import java.time.LocalDateTime

/**
 * An instance of a Knowledge Graph Building Block ([BuildingBlock]) that can be interlinked using
 * [Association]s and [Link]s
 */
data class BuildingBlockInstance(
    val id: BuildingBlockInstanceId,
    val buildingBlockId: BuildingBlockId,
    val createdBy: ContributorId,
    val createdAt: LocalDateTime,
    val associations: MutableMap<AssociationId, Association> = mutableMapOf(),
    val links: MutableMap<LinkId, Link> = mutableMapOf(),
    val references: MutableMap<ReferenceId, Reference> = mutableMapOf()
) {
    val allConnections: Map<ConnectionId, Connection<out ConnectionId>>
        get() = associations + links + references

    val requiredConnections: Collection<Connection<out ConnectionId>>
        get() = allConnections.filter { it.value.minCount > 0 }.values
}
