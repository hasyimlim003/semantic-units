package eu.tib.orkg.buildingblocks.application

import eu.tib.orkg.buildingblocks.api.*
import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.buildingblocks.domain.connection.Association
import eu.tib.orkg.buildingblocks.domain.connection.Link
import eu.tib.orkg.buildingblocks.domain.connection.Reference
import eu.tib.orkg.buildingblocks.spi.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*

@Service
class BuildingBlockInstanceService(
    private val repository: BuildingBlockInstanceRepository,
    private val buildingBlockRepository: BuildingBlockRepository,
    private val associationRepository: AssociationRepository,
    private val linkRepository: LinkRepository,
    private val referenceRepository: ReferenceRepository,
    private val clock: Clock = SystemClock()
) : BuildingBlockInstanceUseCases {
    override fun create(command: CreateBuildingBlockInstanceUseCase.CreateCommand): BuildingBlockInstanceId {
        val buildingBlock = buildingBlockRepository.findById(command.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(command.buildingBlockId) }
        // Validate associations
        command.associations.forEach {
            validateAssociation(
                buildingBlock = buildingBlock,
                targetBuildingBlockInstanceId = it.targetBuildingBlockInstanceId,
                minCount = it.minCount,
                maxCount = it.maxCount,
                carryOverSubjectRangeConstraints = it.carryOverSubjectRangeConstraints
            )
        }
        // Validate links
        command.links.forEach {
            validateLink(
                buildingBlock = buildingBlock,
                targetBuildingBlockInstanceId = it.targetBuildingBlockInstanceId,
                minCount = it.minCount,
                maxCount = it.maxCount,
                useAsSubject = it.useAsSubject
            )
        }
        // Validate references
        command.references.forEach {
            validateReference(
                buildingBlock = buildingBlock,
                targetBuildingBlockInstanceId = it.targetBuildingBlockInstanceId,
                minCount = it.minCount,
                maxCount = it.maxCount
            )
        }
        // Create building block instance
        val id = command.id?.apply {
            repository.findById(this).ifPresent { throw BuildingBlockInstanceAlreadyExists(it.id) }
        } ?: repository.nextIdentity()
        val buildingBlockInstance = BuildingBlockInstance(
            id = id,
            buildingBlockId = buildingBlock.id,
            createdBy = command.contributorId,
            createdAt = clock.now(),
            associations = command.associations.associateTo(mutableMapOf()) {
                val association = Association(
                    id = associationRepository.nextIdentity(),
                    targetBuildingBlockInstance = it.targetBuildingBlockInstanceId,
                    minCount = it.minCount,
                    maxCount = it.maxCount,
                    createdBy = command.contributorId,
                    createdAt = clock.now(),
                    carryOverSubjectRangeConstraints = it.carryOverSubjectRangeConstraints
                )
                association.id to association
            },
            links = command.links.associateTo(mutableMapOf()) {
                val link = Link(
                    id = linkRepository.nextIdentity(),
                    targetBuildingBlockInstance = it.targetBuildingBlockInstanceId,
                    minCount = it.minCount,
                    maxCount = it.maxCount,
                    createdBy = command.contributorId,
                    createdAt = clock.now(),
                    useAsSubject = it.useAsSubject
                )
                link.id to link
            },
            references = command.references.associateTo(mutableMapOf()) {
                val reference = Reference(
                    id = referenceRepository.nextIdentity(),
                    targetBuildingBlockInstance = it.targetBuildingBlockInstanceId,
                    minCount = it.minCount,
                    maxCount = it.maxCount,
                    createdBy = command.contributorId,
                    createdAt = clock.now()
                )
                reference.id to reference
            }
        )
        repository.save(buildingBlockInstance)
        return buildingBlockInstance.id
    }

    override fun findById(buildingBlockId: BuildingBlockInstanceId): Optional<BuildingBlockInstance> =
        repository.findById(buildingBlockId)

    override fun findAll(pageable: Pageable): Page<BuildingBlockInstance> =
        repository.findAll(pageable)

    override fun createAssociation(command: CreateAssociationUseCase.CreateAssociationCommand): AssociationId {
        val sourceBuildingBlockInstance = repository.findById(command.sourceBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(command.sourceBuildingBlockInstanceId) }
        val buildingBlock = buildingBlockRepository.findById(sourceBuildingBlockInstance.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(sourceBuildingBlockInstance.buildingBlockId) }
        val targetBuildingBlockInstance = repository.findById(command.targetBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(command.targetBuildingBlockInstanceId) }
        validateAssociation(
            buildingBlock = buildingBlock,
            targetBuildingBlockInstance = targetBuildingBlockInstance,
            minCount = command.minCount,
            maxCount = command.maxCount,
            carryOverSubjectRangeConstraints = command.carryOverSubjectRangeConstraints
        )
        checkForLoop(command.sourceBuildingBlockInstanceId, targetBuildingBlockInstance)
        val association = Association(
            id = associationRepository.nextIdentity(),
            targetBuildingBlockInstance = command.targetBuildingBlockInstanceId,
            minCount = command.minCount,
            maxCount = command.maxCount,
            createdBy = command.contributorId,
            createdAt = clock.now(),
            carryOverSubjectRangeConstraints = command.carryOverSubjectRangeConstraints
        )
        sourceBuildingBlockInstance.associations[association.id] = association
        repository.save(sourceBuildingBlockInstance)
        return association.id
    }

    override fun createLink(command: CreateLinkUseCase.CreateLinkCommand): LinkId {
        val sourceBuildingBlockInstance = repository.findById(command.sourceBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(command.sourceBuildingBlockInstanceId) }
        val buildingBlock = buildingBlockRepository.findById(sourceBuildingBlockInstance.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(sourceBuildingBlockInstance.buildingBlockId) }
        val targetBuildingBlockInstance = repository.findById(command.targetBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(command.targetBuildingBlockInstanceId) }
        validateLink(
            buildingBlock = buildingBlock,
            targetBuildingBlockInstance = targetBuildingBlockInstance,
            minCount = command.minCount,
            maxCount = command.maxCount,
            useAsSubject = command.useAsSubject
        )
        checkForLoop(command.sourceBuildingBlockInstanceId, targetBuildingBlockInstance)
        val link = Link(
            id = linkRepository.nextIdentity(),
            targetBuildingBlockInstance = command.targetBuildingBlockInstanceId,
            minCount = command.minCount,
            maxCount = command.maxCount,
            createdBy = command.contributorId,
            createdAt = clock.now(),
            useAsSubject = command.useAsSubject
        )
        sourceBuildingBlockInstance.links[link.id] = link
        repository.save(sourceBuildingBlockInstance)
        return link.id
    }

    override fun createReference(command: CreateReferenceUseCase.CreateReferenceCommand): ReferenceId {
        val sourceBuildingBlockInstance = repository.findById(command.sourceBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(command.sourceBuildingBlockInstanceId) }
        val buildingBlock = buildingBlockRepository.findById(sourceBuildingBlockInstance.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(sourceBuildingBlockInstance.buildingBlockId) }
        val targetBuildingBlockInstance = repository.findById(command.targetBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(command.targetBuildingBlockInstanceId) }
        validateReference(
            buildingBlock = buildingBlock,
            targetBuildingBlockInstance = targetBuildingBlockInstance,
            minCount = command.minCount,
            maxCount = command.maxCount
        )
        checkForLoop(command.sourceBuildingBlockInstanceId, targetBuildingBlockInstance)
        val reference = Reference(
            id = referenceRepository.nextIdentity(),
            targetBuildingBlockInstance = command.targetBuildingBlockInstanceId,
            minCount = command.minCount,
            maxCount = command.maxCount,
            createdBy = command.contributorId,
            createdAt = clock.now()
        )
        sourceBuildingBlockInstance.references[reference.id] = reference
        repository.save(sourceBuildingBlockInstance)
        return reference.id
    }

    override fun deleteAll() = repository.deleteAll()

    private fun validateAssociation(
        buildingBlock: BuildingBlock,
        targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        minCount: Int,
        maxCount: Int,
        carryOverSubjectRangeConstraints: Set<ObjectPositionId>
    ) = validateAssociation(
        buildingBlock = buildingBlock,
        targetBuildingBlockInstance = repository.findById(targetBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(targetBuildingBlockInstanceId) },
        minCount = minCount,
        maxCount = maxCount,
        carryOverSubjectRangeConstraints = carryOverSubjectRangeConstraints
    )

    private fun validateAssociation(
        buildingBlock: BuildingBlock,
        targetBuildingBlockInstance: BuildingBlockInstance,
        minCount: Int,
        maxCount: Int,
        carryOverSubjectRangeConstraints: Set<ObjectPositionId>
    ) {
        if (buildingBlock.type == BuildingBlock.Type.STATEMENT) { // buildingBlock.type != BuildingBlock.Type.COMPOUND
            throw UnsupportedOperationException("""Creating associations is currently not supported.""")
        }
        if (targetBuildingBlockInstance.id in Constants.reservedBuildingBlockInstanceIds) {
            throw RuntimeException("""Target building block instance ${targetBuildingBlockInstance.id} is not allowed.""")
        }
        val targetBuildingBlock = buildingBlockRepository.findById(targetBuildingBlockInstance.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(targetBuildingBlockInstance.buildingBlockId) }
        carryOverSubjectRangeConstraints.forEach {
            if (it !in targetBuildingBlock.objectPositionClasses) {
                throw InvalidObjectPosition.forAssociationConstraint(buildingBlock.id, it)
            }
        }
        if (minCount < 0) {
            throw InvalidCountBoundaries.Min.forAssociation(minCount)
        }
        if (maxCount < 0) {
            throw InvalidCountBoundaries.Max.forAssociation(maxCount)
        }
        if (minCount > 0 && maxCount > 0 && maxCount < minCount) {
            throw InvalidCountBoundaries.forAssociation(minCount, maxCount)
        }
    }

    private fun validateLink(
        buildingBlock: BuildingBlock,
        targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        minCount: Int,
        maxCount: Int,
        useAsSubject: ObjectPositionId
    ) = validateLink(
        buildingBlock = buildingBlock,
        targetBuildingBlockInstance = repository.findById(targetBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(targetBuildingBlockInstanceId) },
        minCount = minCount,
        maxCount = maxCount,
        useAsSubject = useAsSubject
    )

    private fun validateLink(
        buildingBlock: BuildingBlock,
        targetBuildingBlockInstance: BuildingBlockInstance,
        minCount: Int,
        maxCount: Int,
        useAsSubject: ObjectPositionId
    ) {
        if (buildingBlock.type != BuildingBlock.Type.STATEMENT) {
            throw RuntimeException("""Source building block of a link must be a statement building block.""")
        }
        if (targetBuildingBlockInstance.id in Constants.reservedBuildingBlockInstanceIds) {
            throw RuntimeException("""Target building block instance ${targetBuildingBlockInstance.id} is not allowed.""")
        }
        val targetBuildingBlock = buildingBlockRepository.findById(targetBuildingBlockInstance.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(targetBuildingBlockInstance.buildingBlockId) }
        if (useAsSubject !in targetBuildingBlock.objectPositionClasses) {
            throw InvalidObjectPosition.forLinkSubject(buildingBlock.id, useAsSubject)
        }
        if (minCount < 0) {
            throw InvalidCountBoundaries.Min.forLink(minCount)
        }
        if (maxCount < 0) {
            throw InvalidCountBoundaries.Max.forLink(maxCount)
        }
        if (minCount > 0 && maxCount > 0 && maxCount < minCount) {
            throw InvalidCountBoundaries.forLink(minCount, maxCount)
        }
    }

    private fun validateReference(
        buildingBlock: BuildingBlock,
        targetBuildingBlockInstanceId: BuildingBlockInstanceId,
        minCount: Int,
        maxCount: Int
    ) = validateReference(
        buildingBlock = buildingBlock,
        targetBuildingBlockInstance = repository.findById(targetBuildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(targetBuildingBlockInstanceId) },
        minCount = minCount,
        maxCount = maxCount
    )

    private fun validateReference(
        buildingBlock: BuildingBlock,
        targetBuildingBlockInstance: BuildingBlockInstance,
        minCount: Int,
        maxCount: Int
    ) {
        if (buildingBlock.type != BuildingBlock.Type.STATEMENT) {
            throw RuntimeException("""Source building block of a reference must be a statement building block.""")
        }
        if (targetBuildingBlockInstance.id in Constants.reservedBuildingBlockInstanceIds) {
            throw RuntimeException("""Target building block instance ${targetBuildingBlockInstance.id} is not allowed.""")
        }
        val targetBuildingBlock = buildingBlockRepository.findById(targetBuildingBlockInstance.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(targetBuildingBlockInstance.buildingBlockId) }
        if (targetBuildingBlock.type != BuildingBlock.Type.STATEMENT) {
            throw RuntimeException("""Target building block of a reference must be a statement building block.""")
        }
        if (minCount < 0) {
            throw InvalidCountBoundaries.Min.forReference(minCount)
        }
        if (maxCount < 0) {
            throw InvalidCountBoundaries.Max.forReference(maxCount)
        }
        if (minCount > 0 && maxCount > 0 && maxCount < minCount) {
            throw InvalidCountBoundaries.forReference(minCount, maxCount)
        }
    }

    private fun checkForLoop(sourceBuildingBlockInstanceId: BuildingBlockInstanceId, targetBuildingBlockInstance: BuildingBlockInstance) {
        val checked: MutableSet<BuildingBlockInstanceId> = mutableSetOf()
        val pending: MutableSet<BuildingBlockInstanceId> = mutableSetOf()
        var buildingBlockInstance: BuildingBlockInstance? = targetBuildingBlockInstance

        do {
            buildingBlockInstance!!.requiredConnections.forEach {
                if (it.targetBuildingBlockInstance == sourceBuildingBlockInstanceId) {
                    throw RuntimeException("Loop in connection")
                }
                if (it.targetBuildingBlockInstance !in checked) {
                    pending.add(it.targetBuildingBlockInstance)
                }
            }
            buildingBlockInstance = pending.firstOrNull()?.let {
                repository.findById(it).orElseThrow { BuildingBlockInstanceNotFound(it) }
            }
        } while (buildingBlockInstance != null)
    }
}
