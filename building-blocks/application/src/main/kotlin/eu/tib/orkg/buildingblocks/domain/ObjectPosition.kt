package eu.tib.orkg.buildingblocks.domain

import java.time.LocalDateTime

sealed interface ObjectPosition {
    val id: ObjectPositionId
    val label: String
    val dataType: DataType
    val required: Boolean
    val constraint: Constraint?
    val createdBy: ContributorId
    val createdAt: LocalDateTime
}

data class IntegerObjectPosition(
    override val id: ObjectPositionId,
    override val label: String,
    override val required: Boolean,
    override val constraint: IntegerConstraint?,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : ObjectPosition {
    override val dataType = DataType.INTEGER
}

data class DecimalObjectPosition(
    override val id: ObjectPositionId,
    override val label: String,
    override val required: Boolean,
    override val constraint: DecimalConstraint?,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : ObjectPosition {
    override val dataType = DataType.DECIMAL
}

data class StringObjectPosition(
    override val id: ObjectPositionId,
    override val label: String,
    override val required: Boolean,
    override val constraint: StringConstraint?,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : ObjectPosition {
    override val dataType = DataType.STRING
}

data class ResourceObjectPosition(
    override val id: ObjectPositionId,
    override val label: String,
    override val required: Boolean,
    override val constraint: ResourceConstraint?,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : ObjectPosition {
    override val dataType = DataType.RESOURCE
}

data class BooleanObjectPosition(
    override val id: ObjectPositionId,
    override val label: String,
    override val required: Boolean,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : ObjectPosition {
    override val constraint = null
    override val dataType = DataType.BOOLEAN
}

data class DateTimeObjectPosition(
    override val id: ObjectPositionId,
    override val label: String,
    override val required: Boolean,
    override val constraint: DateTimeConstraint?,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime
) : ObjectPosition {
    override val dataType = DataType.DATE_TIME
}
