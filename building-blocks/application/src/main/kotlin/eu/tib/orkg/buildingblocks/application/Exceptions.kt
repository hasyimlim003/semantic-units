package eu.tib.orkg.buildingblocks.application

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import eu.tib.orkg.common.application.SimpleMessageException
import org.springframework.http.HttpStatus

class BuildingBlockNotFound(id: BuildingBlockId) :
    SimpleMessageException(HttpStatus.NOT_FOUND, """Building block "$id" not found.""")

class BuildingBlockInstanceNotFound(id: BuildingBlockInstanceId) :
    SimpleMessageException(HttpStatus.NOT_FOUND, """Building block instance "$id" not found.""")

class ObjectPositionNotFound(id: ObjectPositionId) :
    SimpleMessageException(HttpStatus.NOT_FOUND, """Object position "$id" not found.""")

class BuildingBlockAlreadyExists(id: BuildingBlockId) :
    SimpleMessageException(HttpStatus.FORBIDDEN, """Building block "$id" already exists.""")

class BuildingBlockInstanceAlreadyExists(id: BuildingBlockInstanceId) :
    SimpleMessageException(HttpStatus.FORBIDDEN, """Building block instance "$id" already exists.""")

class ObjectPositionAlreadyExists(id: ObjectPositionId) :
    SimpleMessageException(HttpStatus.FORBIDDEN, """Object position "$id" already exists.""")

class InvalidObjectPosition private constructor(
    override val message: String
) : SimpleMessageException(HttpStatus.NOT_FOUND, message) {

    companion object {
        fun forAssociationConstraint(id: BuildingBlockId, objectPositionId: ObjectPositionId) =
            InvalidObjectPosition("""Association references invalid object position "$objectPositionId" for building block "$id".""")

        fun forLinkSubject(id: BuildingBlockId, objectPositionId: ObjectPositionId) =
            InvalidObjectPosition("""Link references invalid object position "$objectPositionId" for building block "$id".""")
    }
}

class InvalidCountBoundaries private constructor(
    override val message: String
) : SimpleMessageException(HttpStatus.FORBIDDEN, message) {

    companion object {
        fun forAssociation(minCount: Int, maxCount: Int) =
            InvalidCountBoundaries("""Invalid count boundaries [min=$minCount, max=$maxCount] for association.""")

        fun forLink(minCount: Int, maxCount: Int) =
            InvalidCountBoundaries("""Invalid count boundaries [min=$minCount, max=$maxCount] for link.""")

        fun forReference(minCount: Int, maxCount: Int) =
            InvalidCountBoundaries("""Invalid count boundaries [min=$minCount, max=$maxCount] for reference.""")
    }

    object Max {
        fun forAssociation(count: Int) =
            InvalidCountBoundaries("""Invalid minimum count "$count" for association.""")

        fun forLink(count: Int) =
            InvalidCountBoundaries("""Invalid minimum count "$count" for link.""")

        fun forReference(count: Int) =
            InvalidCountBoundaries("""Invalid minimum count "$count" for link.""")
    }

    object Min {
        fun forAssociation(count: Int) =
            InvalidCountBoundaries("""Invalid minimum count "$count" for association.""")

        fun forLink(count: Int) =
            InvalidCountBoundaries("""Invalid minimum count "$count" for link.""")

        fun forReference(count: Int) =
            InvalidCountBoundaries("""Invalid minimum count "$count" for reference.""")
    }
}
