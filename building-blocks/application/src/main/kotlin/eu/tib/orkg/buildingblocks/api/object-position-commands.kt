package eu.tib.orkg.buildingblocks.api

import eu.tib.orkg.buildingblocks.domain.Constraint
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.buildingblocks.domain.DataType
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId

interface CreateObjectPositionUseCase {
    fun create(command: CreateCommand): ObjectPositionId

    data class CreateCommand(
        val id: ObjectPositionId? = null,
        val type: DataType,
        val label: String,
        val required: Boolean,
        val contributorId: ContributorId,
        val constraint: Constraint?
    )
}

interface DeleteObjectPositionUseCase {
    fun deleteAll()
}
