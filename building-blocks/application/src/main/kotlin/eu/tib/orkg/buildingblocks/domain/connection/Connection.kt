package eu.tib.orkg.buildingblocks.domain.connection

import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import eu.tib.orkg.buildingblocks.domain.ConnectionId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import java.time.LocalDateTime

sealed interface Connection<T: ConnectionId> {
    val id: T
    val targetBuildingBlockInstance: BuildingBlockInstanceId
    val minCount: Int
    val maxCount: Int
    val createdBy: ContributorId
    val createdAt: LocalDateTime
}
