package eu.tib.orkg.buildingblocks.spi

import eu.tib.orkg.buildingblocks.domain.BuildingBlock
import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.common.spi.EntityRepository

interface BuildingBlockRepository : EntityRepository<BuildingBlockId, BuildingBlock>
