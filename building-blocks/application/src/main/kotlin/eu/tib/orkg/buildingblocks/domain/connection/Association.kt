package eu.tib.orkg.buildingblocks.domain.connection

import eu.tib.orkg.buildingblocks.domain.*
import java.time.LocalDateTime

data class Association(
    override val id: AssociationId,
    override val targetBuildingBlockInstance: BuildingBlockInstanceId,
    override val minCount: Int,
    override val maxCount: Int,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    val carryOverSubjectRangeConstraints: Set<ObjectPositionId>
) : Connection<AssociationId>
