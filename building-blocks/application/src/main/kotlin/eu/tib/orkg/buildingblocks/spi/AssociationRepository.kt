package eu.tib.orkg.buildingblocks.spi

import eu.tib.orkg.buildingblocks.domain.AssociationId
import eu.tib.orkg.common.spi.IdRepository

interface AssociationRepository : IdRepository<AssociationId>
