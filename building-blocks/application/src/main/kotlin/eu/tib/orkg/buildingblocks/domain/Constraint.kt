package eu.tib.orkg.buildingblocks.domain

import java.net.URI
import java.time.LocalDateTime
import java.util.regex.PatternSyntaxException

sealed interface Constraint

open class ComparableConstraint<T>(
    val min: T?,
    val max: T?,
    val minExclusive: Boolean,
    val maxExclusive: Boolean
) : Constraint where T: Comparable<T> {
    init {
        if (min != null && max != null) {
            if (min > max) {
                throw RuntimeException("Minimum value must be equal or less than maximum value.")
            } else if (min == max && (minExclusive || maxExclusive)) {
                throw RuntimeException("Constraint cannot match any value.")
            }
        }
    }
}

class IntegerConstraint(
    min: Long?,
    max: Long?,
    minExclusive: Boolean,
    maxExclusive: Boolean
) : ComparableConstraint<Long>(min, max, minExclusive, maxExclusive)

class DecimalConstraint(
    min: Double?,
    max: Double?,
    minExclusive: Boolean,
    maxExclusive: Boolean
) : ComparableConstraint<Double>(min, max, minExclusive, maxExclusive)

data class StringConstraint(
    val pattern: String
) : Constraint {
    init {
        if (pattern.isBlank()) {
            throw RuntimeException("Pattern must not be blank.")
        }
        try {
            Regex(pattern)
        } catch (e: PatternSyntaxException) {
            throw RuntimeException("Invalid regex pattern.")
        }
    }
}

data class ResourceConstraint(
    val ancestor: URI
) : Constraint

class DateTimeConstraint(
    min: LocalDateTime?,
    max: LocalDateTime?,
    minExclusive: Boolean,
    maxExclusive: Boolean
) : ComparableConstraint<LocalDateTime>(min, max, minExclusive, maxExclusive)
