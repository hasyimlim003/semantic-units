package eu.tib.orkg.buildingblocks.domain

object Constants {
    object NamedIndividualIdentificationUnit {
        val buildingBlockId = BuildingBlockId("NamedIndividualIdentificationUnitBuildingBlock")
        val buildingBlockInstanceId = BuildingBlockInstanceId("NamedIndividualIdentificationUnitBuildingBlockInstance")
        val classIdentificationUnitLinkId = LinkId("NamedIndividualIdentificationUnit2ClassIdentificationUnitLink")
        object ObjectPosition {
            val `class` = ObjectPositionId("NamedIndividualIdentificationUnitClassPosition")
            val label = ObjectPositionId("NamedIndividualIdentificationUnitLabelPosition")
        }
    }

    object SomeInstanceIdentificationUnit {
        val buildingBlockId = BuildingBlockId("SomeInstanceIdentificationUnitBuildingBlock")
        val buildingBlockInstanceId = BuildingBlockInstanceId("SomeInstanceIdentificationUnitBuildingBlockInstance")
        val classIdentificationUnitLinkId = LinkId("SomeInstanceIdentificationUnit2ClassIdentificationUnitLink")
        object ObjectPosition {
            val `class` = ObjectPositionId("SomeInstanceIdentificationUnitClassPosition")
            val label = ObjectPositionId("SomeInstanceIdentificationUnitLabelPosition")
        }
    }

    object EveryInstanceIdentificationUnit {
        val buildingBlockId = BuildingBlockId("EveryInstanceIdentificationUnitBuildingBlock")
        val buildingBlockInstanceId = BuildingBlockInstanceId("EveryInstanceIdentificationUnitBuildingBlockInstance")
        val classIdentificationUnitLinkId = LinkId("EveryInstanceIdentificationUnit2ClassIdentificationUnitLink")
        object ObjectPosition {
            val `class` = ObjectPositionId("EveryInstanceIdentificationClassPosition")
            val label = ObjectPositionId("EveryInstanceIdentificationUnitLabelPosition")
        }
    }

    object ClassIdentificationUnit {
        val buildingBlockId = BuildingBlockId("ClassIdentificationUnitBuildingBlock")
        val buildingBlockInstanceId = BuildingBlockInstanceId("ClassIdentificationUnitBuildingBlockInstance")
        object ObjectPosition {
            val classUri = ObjectPositionId("ClassIdentificationClassUriPosition")
            val label = ObjectPositionId("ClassIdentificationUnitLabelPosition")
            val ontology = ObjectPositionId("ClassIdentificationUnitOntologyPosition")
        }
    }

    val instanceIdentificationUnitIds = setOf(
        NamedIndividualIdentificationUnit.buildingBlockId,
        SomeInstanceIdentificationUnit.buildingBlockId,
        EveryInstanceIdentificationUnit.buildingBlockId
    )

    val identificationUnitIds = instanceIdentificationUnitIds + ClassIdentificationUnit.buildingBlockId

    val reservedBuildingBlockIds = setOf(
        NamedIndividualIdentificationUnit.buildingBlockId,
        SomeInstanceIdentificationUnit.buildingBlockId,
        EveryInstanceIdentificationUnit.buildingBlockId,
        ClassIdentificationUnit.buildingBlockId
    )

    val reservedBuildingBlockInstanceIds = setOf(
        NamedIndividualIdentificationUnit.buildingBlockInstanceId,
        SomeInstanceIdentificationUnit.buildingBlockInstanceId,
        EveryInstanceIdentificationUnit.buildingBlockInstanceId,
        ClassIdentificationUnit.buildingBlockInstanceId
    )

    val reservedObjectPositionIds = setOf(
        NamedIndividualIdentificationUnit.ObjectPosition.label,
        NamedIndividualIdentificationUnit.ObjectPosition.`class`,

        SomeInstanceIdentificationUnit.ObjectPosition.label,
        SomeInstanceIdentificationUnit.ObjectPosition.`class`,

        EveryInstanceIdentificationUnit.ObjectPosition.label,
        EveryInstanceIdentificationUnit.ObjectPosition.`class`,

        ClassIdentificationUnit.ObjectPosition.classUri,
        ClassIdentificationUnit.ObjectPosition.label,
        ClassIdentificationUnit.ObjectPosition.ontology
    )
}
