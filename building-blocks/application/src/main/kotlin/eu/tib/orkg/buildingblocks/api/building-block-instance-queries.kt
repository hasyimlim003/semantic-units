package eu.tib.orkg.buildingblocks.api

import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstance
import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.util.Optional

interface RetrieveBuildingBlockInstanceUseCase {
    fun findById(buildingBlockId: BuildingBlockInstanceId): Optional<BuildingBlockInstance>
    fun findAll(pageable: Pageable): Page<BuildingBlockInstance>
}
