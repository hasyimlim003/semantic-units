package eu.tib.orkg.buildingblocks.spi

import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstance
import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import eu.tib.orkg.common.spi.EntityRepository

interface BuildingBlockInstanceRepository : EntityRepository<BuildingBlockInstanceId, BuildingBlockInstance>
