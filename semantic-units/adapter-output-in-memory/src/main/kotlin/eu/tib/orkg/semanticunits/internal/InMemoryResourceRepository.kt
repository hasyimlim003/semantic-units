package eu.tib.orkg.semanticunits.internal

import eu.tib.orkg.semanticunits.domain.ResourceId
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryResourceRepository {
    private val ids: Set<ResourceId> = mutableSetOf()

    fun nextIdentity(): ResourceId {
        var id = ResourceId(UUID.randomUUID().toString())
        while(id in ids) {
            id = ResourceId(UUID.randomUUID().toString())
        }
        return id
    }
}
