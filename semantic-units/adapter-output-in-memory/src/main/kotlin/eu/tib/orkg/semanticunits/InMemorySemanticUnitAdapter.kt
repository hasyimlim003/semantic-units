package eu.tib.orkg.semanticunits

import eu.tib.orkg.semanticunits.domain.ResourceId
import eu.tib.orkg.semanticunits.domain.SemanticUnit
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.identification.ClassIdentificationUnit
import eu.tib.orkg.semanticunits.domain.identification.IdentificationUnit
import eu.tib.orkg.semanticunits.internal.InMemorySemanticUnitRepository
import eu.tib.orkg.semanticunits.spi.SemanticUnitRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*

@Component
class InMemorySemanticUnitAdapter(
    private val repository: InMemorySemanticUnitRepository
): SemanticUnitRepository {
    override fun save(entity: SemanticUnit) = repository.save(entity)

    override fun findAll(pageable: Pageable): Page<SemanticUnit> = repository.findAll(pageable)

    override fun findById(id: SemanticUnitId): Optional<SemanticUnit> = repository.findById(id)

    override fun findIdentificationUnitBySubjectId(id: ResourceId): Optional<IdentificationUnit> =
        repository.findIdentificationUnitBySubjectId(id)

    override fun findClassIdentificationUnitByClassUri(uri: URI): Optional<ClassIdentificationUnit> =
        repository.findClassIdentificationUnitByClassUri(uri)

    override fun deleteAll() = repository.deleteAll()

    override fun nextIdentity(): SemanticUnitId = repository.nextIdentity()
}
