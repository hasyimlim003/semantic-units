package eu.tib.orkg.semanticunits.internal

import eu.tib.orkg.buildingblocks.domain.Constants
import eu.tib.orkg.common.adapter.output.inmemory.InMemoryRepository
import eu.tib.orkg.semanticunits.domain.ResourceId
import eu.tib.orkg.semanticunits.domain.SemanticUnit
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.StringLiteralObjectNode
import eu.tib.orkg.semanticunits.domain.identification.ClassIdentificationUnit
import eu.tib.orkg.semanticunits.domain.identification.IdentificationUnit
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*

@Component
class InMemorySemanticUnitRepository : InMemoryRepository<SemanticUnitId, SemanticUnit>(
    idSelector = SemanticUnit::id,
    defaultComparator = compareBy(SemanticUnit::id)
) {
    override fun nextIdentity(): SemanticUnitId {
        var id = SemanticUnitId(UUID.randomUUID().toString())
        while(id in entities) {
            id = SemanticUnitId(UUID.randomUUID().toString())
        }
        return id
    }

    fun findIdentificationUnitBySubjectId(id: ResourceId): Optional<IdentificationUnit> =
        Optional.ofNullable(entities.values.singleOrNull { it is IdentificationUnit && it.subject == id } as IdentificationUnit)

    fun findClassIdentificationUnitByClassUri(uri: URI): Optional<ClassIdentificationUnit> =
        Optional.ofNullable(entities.values.singleOrNull {
            it is ClassIdentificationUnit && it.objectNodes.current[Constants.ClassIdentificationUnit.ObjectPosition.classUri]?.let { objectNode ->
                URI.create((objectNode as StringLiteralObjectNode).value) == uri
            } ?: false
        }).map { it as ClassIdentificationUnit }
}
