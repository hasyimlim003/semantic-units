package eu.tib.orkg.semanticunits

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.semanticunits.spi.OntologyRepository
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*

@Component
class InMemoryOntologyAdapter : OntologyRepository {
    override fun findByURI(uri: URI): Optional<OntologyRepository.Term> = Optional.of(
        OntologyRepository.Term(
            uri = uri,
            label = "Label",
            ontology = "Ontology"
        )
    )

    override fun isSubclassOf(constraint: URI, uri: URI): Boolean {
        return true
    }

    override fun isSubclassOf(constraint: BuildingBlockId, buildingBlockId: BuildingBlockId): Boolean {
        return true
    }
}
