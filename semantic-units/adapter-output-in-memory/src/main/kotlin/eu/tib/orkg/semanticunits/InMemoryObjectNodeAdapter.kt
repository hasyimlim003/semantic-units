package eu.tib.orkg.semanticunits

import eu.tib.orkg.semanticunits.domain.ObjectNodeId
import eu.tib.orkg.semanticunits.internal.InMemoryObjectNodeRepository
import eu.tib.orkg.semanticunits.spi.ObjectNodeRepository
import org.springframework.stereotype.Component

@Component
class InMemoryObjectNodeAdapter(
    private val repository: InMemoryObjectNodeRepository
): ObjectNodeRepository {
    override fun nextIdentity(): ObjectNodeId = repository.nextIdentity()
}
