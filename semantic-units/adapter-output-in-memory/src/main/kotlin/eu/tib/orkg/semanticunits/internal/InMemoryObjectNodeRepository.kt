package eu.tib.orkg.semanticunits.internal

import eu.tib.orkg.semanticunits.domain.ObjectNodeId
import org.springframework.stereotype.Component
import java.util.*

@Component
class InMemoryObjectNodeRepository {
    private val ids: Set<ObjectNodeId> = mutableSetOf()

    fun nextIdentity(): ObjectNodeId {
        var id = ObjectNodeId(UUID.randomUUID().toString())
        while(id in ids) {
            id = ObjectNodeId(UUID.randomUUID().toString())
        }
        return id
    }
}
