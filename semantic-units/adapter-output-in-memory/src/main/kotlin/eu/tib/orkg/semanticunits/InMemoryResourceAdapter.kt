package eu.tib.orkg.semanticunits

import eu.tib.orkg.semanticunits.domain.ResourceId
import eu.tib.orkg.semanticunits.internal.InMemoryResourceRepository
import eu.tib.orkg.semanticunits.spi.ResourceRepository
import org.springframework.stereotype.Component

@Component
class InMemoryResourceAdapter(
    private val repository: InMemoryResourceRepository
): ResourceRepository {
    override fun nextIdentity(): ResourceId = repository.nextIdentity()
}
