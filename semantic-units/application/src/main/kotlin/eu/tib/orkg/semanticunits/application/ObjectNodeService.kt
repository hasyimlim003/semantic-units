package eu.tib.orkg.semanticunits.application

import eu.tib.orkg.buildingblocks.application.ObjectPositionNotFound
import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.buildingblocks.spi.ObjectPositionRepository
import eu.tib.orkg.semanticunits.domain.*
import eu.tib.orkg.semanticunits.domain.identification.ClassIdentificationUnit
import eu.tib.orkg.semanticunits.domain.identification.InstanceIdentificationUnit
import eu.tib.orkg.semanticunits.spi.ObjectNodeRepository
import eu.tib.orkg.semanticunits.spi.SemanticUnitRepository
import org.springframework.stereotype.Service
import java.net.URI
import java.time.LocalDateTime

@Service
class ObjectNodeService(
    private val objectPositionRepository: ObjectPositionRepository,
    private val objectNodeRepository: ObjectNodeRepository,
    private val semanticUnitRepository: SemanticUnitRepository,
    private val ontologyService: OntologyService,
    private val clock: Clock = SystemClock()
) {
    data class CreateCommand(
        val input: ObjectPositionInput,
        val objectPosition: ObjectPosition,
        val contributorId: ContributorId
    ) {
        sealed interface ObjectPositionInput
        sealed interface ResourcePositionInput : ObjectPositionInput

        data class ExistingResourcePositionInput(
            val resource: SubjectId,
            val carriedOverSubjectRangeConstraint: ObjectPositionId?
        ): ResourcePositionInput

        data class NewResourcePositionInput(
            val instanceIdentificationUnit: InstanceIdentificationUnit,
            val classIdentificationUnit: ClassIdentificationUnit,
            val carriedOverSubjectRangeConstraint: ObjectPositionId?
        ): ResourcePositionInput

        data class LiteralPositionInput(
            val value: String,
        ): ObjectPositionInput
    }

    fun create(command: CreateCommand): ObjectNode<*> = with(command) {
        when (input) {
            is CreateCommand.ResourcePositionInput ->
                when (objectPosition) {
                    is ResourceObjectPosition -> ResourceObjectNode(
                        id = objectNodeRepository.nextIdentity(),
                        objectPositionClass = objectPosition.id,
                        createdBy = contributorId,
                        createdAt = clock.now(),
                        value = when (input) {
                            is CreateCommand.ExistingResourcePositionInput -> {
                                objectPosition.parseAndValidate(
                                    value = input.resource,
                                    carriedOverSubjectRangeConstraint = input.carriedOverSubjectRangeConstraint
                                )
                            }
                            is CreateCommand.NewResourcePositionInput -> {
                                objectPosition.parseAndValidate(
                                    instanceIdentificationUnit = input.instanceIdentificationUnit,
                                    classIdentificationUnit = input.classIdentificationUnit,
                                    carriedOverSubjectRangeConstraint = input.carriedOverSubjectRangeConstraint
                                )
                            }
                        }
                    )
                    else -> throw RuntimeException("Invalid input for object position ${objectPosition.id}. Expected literal input but got resource input.")
                }
            is CreateCommand.LiteralPositionInput ->
                when (objectPosition) {
                    is StringObjectPosition -> StringLiteralObjectNode(
                        id = objectNodeRepository.nextIdentity(),
                        objectPositionClass = objectPosition.id,
                        createdBy = contributorId,
                        createdAt = clock.now(),
                        value = objectPosition.parseAndValidate(input.value)
                    )
                    is IntegerObjectPosition -> IntegerLiteralObjectNode(
                        id = objectNodeRepository.nextIdentity(),
                        objectPositionClass = objectPosition.id,
                        createdBy = contributorId,
                        createdAt = clock.now(),
                        value = objectPosition.parseAndValidate(input.value)
                    )
                    is DecimalObjectPosition -> DecimalLiteralObjectNode(
                        id = objectNodeRepository.nextIdentity(),
                        objectPositionClass = objectPosition.id,
                        createdBy = contributorId,
                        createdAt = clock.now(),
                        value = objectPosition.parseAndValidate(input.value)
                    )
                    is BooleanObjectPosition -> BooleanLiteralObjectNode(
                        id = objectNodeRepository.nextIdentity(),
                        objectPositionClass = objectPosition.id,
                        createdBy = contributorId,
                        createdAt = clock.now(),
                        value = objectPosition.parseAndValidate(input.value)
                    )
                    is DateTimeObjectPosition -> DateTimeLiteralObjectNode(
                        id = objectNodeRepository.nextIdentity(),
                        objectPositionClass = objectPosition.id,
                        createdBy = contributorId,
                        createdAt = clock.now(),
                        value = objectPosition.parseAndValidate(input.value)
                    )
                    else -> throw RuntimeException("Invalid input for object position ${objectPosition.id}. Expected resource input but got literal input.")
                }
        }
    }

    // Parsers

    private fun ResourceObjectPosition.parseAndValidate(
        instanceIdentificationUnit: InstanceIdentificationUnit,
        classIdentificationUnit: ClassIdentificationUnit,
        carriedOverSubjectRangeConstraint: ObjectPositionId?
    ): SubjectId {
        if (carriedOverSubjectRangeConstraint != null) {
            val carriedObjectPosition = objectPositionRepository.findById(carriedOverSubjectRangeConstraint)
                .orElseThrow { ObjectPositionNotFound(carriedOverSubjectRangeConstraint) }
            if (carriedObjectPosition !is ResourceObjectPosition) {
                throw RuntimeException("""Expected object position "$carriedObjectPosition" to be a resource object position.""")
            }
            carriedObjectPosition.constraint?.validate(classIdentificationUnit)
        }
        constraint?.validate(classIdentificationUnit)
        return instanceIdentificationUnit.subject
    }

    private fun ResourceObjectPosition.parseAndValidate(
        value: SubjectId,
        carriedOverSubjectRangeConstraint: ObjectPositionId?
    ) = when (value) {
        is ResourceId -> {
            val identificationUnit = semanticUnitRepository.findIdentificationUnitBySubjectId(value)
                .orElseThrow { SubjectResourceNotFound(value) }
            if (identificationUnit !is InstanceIdentificationUnit) {
                throw RuntimeException("""""")
            }
            val classObjectPositionId = identificationUnit.instanceType.classObjectPositionId
            val classObjectPositionNode = identificationUnit.objectNodes.current[classObjectPositionId] as ResourceObjectNode
            val classIdentificationUnit = semanticUnitRepository.findIdentificationUnitBySubjectId(classObjectPositionNode.value as ResourceId)
                .orElseThrow { SubjectResourceNotFound(value) } as ClassIdentificationUnit
            parseAndValidate(identificationUnit, classIdentificationUnit, carriedOverSubjectRangeConstraint)
        }
        is SemanticUnitId -> {
            // TODO: validate constraint
            semanticUnitRepository.findById(value)
                .map { it.id }
                .orElseThrow { SubjectResourceNotFound(value) }
        }
    }

    private fun StringObjectPosition.parseAndValidate(value: String): String {
        constraint?.validate(value)
        return value
    }

    private fun DecimalObjectPosition.parseAndValidate(value: String): Double {
        val parsedValue = value.toDouble()
        constraint?.validate(parsedValue)
        return parsedValue
    }

    private fun IntegerObjectPosition.parseAndValidate(value: String): Long {
        val parsedValue = value.toLong()
        constraint?.validate(parsedValue)
        return parsedValue
    }

    private fun BooleanObjectPosition.parseAndValidate(value: String) = value.toBoolean()

    private fun DateTimeObjectPosition.parseAndValidate(value: String): LocalDateTime {
        val parsedValue = LocalDateTime.parse(value)
        constraint?.validate(parsedValue)
        return parsedValue
    }
    
    // Validators

    private fun ResourceConstraint.validate(value: ClassIdentificationUnit) {
        val classUriNode = value.objectNodes.current[Constants.ClassIdentificationUnit.ObjectPosition.classUri] as StringLiteralObjectNode
        val classUri = URI.create(classUriNode.value)

        if (!ontologyService.isSubclassOf(ancestor, classUri)) {
            throw RuntimeException(""""$classUri" has to be a subclass of "$ancestor".""")
        }
    }

    private fun StringConstraint.validate(value: String) {
        if (value.matches(pattern.toRegex())) {
            throw RuntimeException("""String "$value" does not match pattern $pattern""")
        }
    }

    private fun <T> ComparableConstraint<T>.validate(value: T) where T: Comparable<T> {
        if (min != null) {
            val compareResult = min!!.compareTo(value)
            if (compareResult == 0 && minExclusive) {
                throw RuntimeException("""Value $value is less than minimum allowed value $min (min exclusive)""")
            } else if (compareResult < 0) {
                throw RuntimeException("""Value $value is less than minimum allowed value $min""")
            }
        }
        if (max != null) {
            val compareResult = max!!.compareTo(value)
            if (compareResult == 0 && maxExclusive) {
                throw RuntimeException("""Value $value is higher than minimum allowed value $max (max exclusive)""")
            } else if (compareResult > 0) {
                throw RuntimeException("""Value $value is higher than minimum allowed value $max""")
            }
        }
    }
}
