package eu.tib.orkg.semanticunits.spi

import eu.tib.orkg.semanticunits.domain.ResourceId
import eu.tib.orkg.common.spi.IdRepository

interface ResourceRepository : IdRepository<ResourceId>
