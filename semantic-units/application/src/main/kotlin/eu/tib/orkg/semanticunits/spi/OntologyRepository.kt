package eu.tib.orkg.semanticunits.spi

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import java.net.URI
import java.util.*

interface OntologyRepository {
    fun findByURI(uri: URI): Optional<Term>
    fun isSubclassOf(constraint: URI, uri: URI): Boolean
    fun isSubclassOf(constraint: BuildingBlockId, buildingBlockId: BuildingBlockId): Boolean

    data class Term(
        val uri: URI,
        val label: String,
        val ontology: String
    )
}
