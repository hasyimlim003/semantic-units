package eu.tib.orkg.semanticunits.api

import eu.tib.orkg.buildingblocks.domain.BuildingBlockInstanceId
import eu.tib.orkg.buildingblocks.domain.ConnectionId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.StatementUnit
import eu.tib.orkg.semanticunits.domain.SubjectId
import eu.tib.orkg.semanticunits.domain.identification.InstanceIdentificationUnit
import java.net.URI

interface CreateSemanticUnitUseCase {
    fun create(command: CreateCommand): SemanticUnitId

    data class CreateCommand(
        val id: SemanticUnitId? = null,
        val subject: ResourceInput,
        val contributorId: ContributorId,
        val buildingBlockInstanceId: BuildingBlockInstanceId,
        val type: StatementUnit.Type?,
        val inputs: Map<ObjectPositionId, ObjectPositionInput>,
        val children: List<SemanticUnitInput>
    )
}

interface UpdateSemanticUnitUseCase {
    fun update(command: UpdateCommand)

    data class UpdateCommand(
        val id: SemanticUnitId,
        val contributorId: ContributorId,
        val inputs: Map<ObjectPositionId, ObjectPositionInput>,
        val children: List<SemanticUnitInput>
    )
}

interface DeleteSemanticUnitUseCase {
    fun deleteAll()
}

data class SemanticUnitInput(
    val connectionId: ConnectionId,
    val type: StatementUnit.Type?,
    val inputs: Map<ObjectPositionId, ObjectPositionInput>,
    val children: List<SemanticUnitInput>
)

sealed interface ObjectPositionInput

sealed interface ResourceInput : ObjectPositionInput

data class NewResourceInput(
    val `class`: URI,
    val label: String?,
    val instanceType: InstanceIdentificationUnit.Type
) : ResourceInput

data class ExistingResourceInput(
    val resource: SubjectId
) : ResourceInput

data class LiteralObjectPositionInput(
    val value: String
) : ObjectPositionInput
