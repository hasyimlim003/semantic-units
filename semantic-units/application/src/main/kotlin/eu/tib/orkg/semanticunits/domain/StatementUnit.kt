package eu.tib.orkg.semanticunits.domain

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import eu.tib.orkg.semanticunits.application.ForbiddenModification
import java.lang.IllegalArgumentException
import java.time.LocalDateTime

abstract class StatementUnit(
    id: SemanticUnitId,
    subject: SubjectId,
    buildingBlockId: BuildingBlockId,
    createdBy: ContributorId,
    createdAt: LocalDateTime,
    currentNodes: Set<ObjectNode<*>> = mutableSetOf(),
    deletedNodes: Set<ObjectNode<*>> = mutableSetOf(),
    val type: Type
) : SemanticUnit(id, subject, buildingBlockId, createdBy, createdAt) {
    val objectNodes = ObjectNodes(
        currentNodes = currentNodes.associateByTo(mutableMapOf()) { it.objectPositionClass },
        deletedNodes = deletedNodes.associateByTo(mutableMapOf()) { it.id }
    )

    inner class ObjectNodes(
        private val currentNodes: MutableMap<ObjectPositionId, ObjectNode<*>>,
        private val deletedNodes: MutableMap<ObjectNodeId, ObjectNode<*>>
    ) {
        init {
            val duplicates = currentNodes.values.toSet() intersect deletedNodes.values.toSet()
            if (duplicates.isNotEmpty()) {
                throw IllegalArgumentException("""Object nodes "$duplicates" cannot be current and deleted at the same time."""")
            }
        }

        fun put(node: ObjectNode<*>) {
            if (!isEditable) {
                throw ForbiddenModification(id)
            }
            if (node.objectPositionClass in currentNodes) {
                deletedNodes[node.id] = currentNodes[node.objectPositionClass]!!
            }
            currentNodes[node.objectPositionClass] = node
        }

        val current: Map<ObjectPositionId, ObjectNode<*>>
            get() = currentNodes

        val deleted: Set<ObjectNode<*>>
            get() = deletedNodes.values.toSet()

        val all: Set<ObjectNode<*>>
            get() = (currentNodes.values + deletedNodes.values).toSet()
    }

    enum class Type {
        ASSERTIONAL, // INDIVIDUAL -> INDIVIDUAL
        CONTINGENT, // SOME -> SOME
        PROTOTYPICAL, // SOME -> SOME
        UNIVERSAL, // EVERY -> SOME
        LEXICAL // Identification Units
    }
}
