package eu.tib.orkg.semanticunits.spi

import eu.tib.orkg.semanticunits.domain.ResourceId
import eu.tib.orkg.semanticunits.domain.SemanticUnit
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.identification.ClassIdentificationUnit
import eu.tib.orkg.semanticunits.domain.identification.IdentificationUnit
import eu.tib.orkg.common.spi.EntityRepository
import java.net.URI
import java.util.*

interface SemanticUnitRepository : EntityRepository<SemanticUnitId, SemanticUnit> {
    fun saveAll(semanticUnits: List<SemanticUnit>) = semanticUnits.forEach(::save)
    fun findIdentificationUnitBySubjectId(id: ResourceId): Optional<IdentificationUnit>
    fun findClassIdentificationUnitByClassUri(uri: URI): Optional<ClassIdentificationUnit>
}
