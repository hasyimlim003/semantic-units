package eu.tib.orkg.semanticunits.api

import eu.tib.orkg.semanticunits.domain.SemanticUnit
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.util.*

interface RetrieveSemanticUnitUseCase {
    fun findById(semanticUnitId: SemanticUnitId): Optional<SemanticUnit>
    fun findAll(pageable: Pageable): Page<SemanticUnit>
}
