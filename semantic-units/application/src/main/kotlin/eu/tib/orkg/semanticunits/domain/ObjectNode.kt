package eu.tib.orkg.semanticunits.domain

import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import java.net.URI
import java.time.LocalDateTime

sealed interface ObjectNode<out T> {
    val id: ObjectNodeId
    val objectPositionClass: ObjectPositionId
    val createdBy: ContributorId
    val createdAt: LocalDateTime
    val value: T
}

sealed interface LiteralObjectNode<out T> : ObjectNode<T>

data class StringLiteralObjectNode(
    override val id: ObjectNodeId,
    override val objectPositionClass: ObjectPositionId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val value: String
) : LiteralObjectNode<String>

data class BooleanLiteralObjectNode(
    override val id: ObjectNodeId,
    override val objectPositionClass: ObjectPositionId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val value: Boolean
) : LiteralObjectNode<Boolean>

data class IntegerLiteralObjectNode(
    override val id: ObjectNodeId,
    override val objectPositionClass: ObjectPositionId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val value: Long
) : LiteralObjectNode<Long>

data class DecimalLiteralObjectNode(
    override val id: ObjectNodeId,
    override val objectPositionClass: ObjectPositionId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val value: Double
) : LiteralObjectNode<Double>

data class DateTimeLiteralObjectNode(
    override val id: ObjectNodeId,
    override val objectPositionClass: ObjectPositionId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val value: LocalDateTime
) : LiteralObjectNode<LocalDateTime>

data class ResourceObjectNode(
    override val id: ObjectNodeId,
    override val objectPositionClass: ObjectPositionId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val value: SubjectId
) : ObjectNode<SubjectId>
