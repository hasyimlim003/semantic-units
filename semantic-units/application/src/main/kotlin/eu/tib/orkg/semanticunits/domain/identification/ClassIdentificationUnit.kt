package eu.tib.orkg.semanticunits.domain.identification

import eu.tib.orkg.buildingblocks.domain.Constants
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.semanticunits.domain.ObjectNode
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.SubjectId
import java.time.LocalDateTime

class ClassIdentificationUnit(
    id: SemanticUnitId,
    subject: SubjectId,
    createdBy: ContributorId,
    createdAt: LocalDateTime,
    currentNodes: Set<ObjectNode<*>> = mutableSetOf(),
    deletedNodes: Set<ObjectNode<*>> = mutableSetOf(),
) : IdentificationUnit(
    id, subject, Constants.ClassIdentificationUnit.buildingBlockId, createdBy, createdAt, currentNodes, deletedNodes
)
