package eu.tib.orkg.semanticunits.domain

import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.semanticunits.api.ExistingResourceInput
import eu.tib.orkg.semanticunits.api.LiteralObjectPositionInput
import eu.tib.orkg.semanticunits.api.NewResourceInput
import eu.tib.orkg.semanticunits.api.ObjectPositionInput
import eu.tib.orkg.semanticunits.application.IdentificationUnitNotFound
import eu.tib.orkg.semanticunits.application.SemanticUnitAlreadyExists
import eu.tib.orkg.semanticunits.domain.identification.ClassIdentificationUnit
import eu.tib.orkg.semanticunits.domain.identification.IdentificationUnit
import eu.tib.orkg.semanticunits.domain.identification.InstanceIdentificationUnit
import eu.tib.orkg.semanticunits.application.ObjectNodeService
import eu.tib.orkg.semanticunits.application.SemanticUnitService
import eu.tib.orkg.semanticunits.spi.SemanticUnitRepository
import eu.tib.orkg.semanticunits.spi.ResourceRepository
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
class StatementUnitDomainService(
    private val repository: SemanticUnitRepository,
    @Lazy private val semanticUnitService: SemanticUnitService,
    private val resourceRepository: ResourceRepository,
    private val objectNodeService: ObjectNodeService,
    private val clock: Clock = SystemClock()
) {
    fun create(
        id: SemanticUnitId?,
        subject: StatementUnit?,
        buildingBlock: BuildingBlock,
        contributorId: ContributorId,
        type: StatementUnit.Type,
        inputs: Map<ObjectPositionId, ObjectPositionInput>,
        carriedOverSubjectRangeConstraints: Map<ObjectPositionId, ObjectPositionId>
    ): SemanticUnitService.CreateResult {
        val statementUnitId = id?.apply {
            repository.findById(this).ifPresent { throw SemanticUnitAlreadyExists(it.id) }
        } ?: repository.nextIdentity()
        val statementUnit = SubjectConstrainedStatementUnit(
            id = statementUnitId,
            subject = subject?.id ?: resourceRepository.nextIdentity(),
            buildingBlockId = buildingBlock.id,
            createdBy = contributorId,
            createdAt = clock.now(),
            type = type,
            carriedOverSubjectRangeConstraints = carriedOverSubjectRangeConstraints
        )
        val children = processInputs(
            statementUnit = statementUnit,
            subject = subject,
            buildingBlock = buildingBlock,
            contributorId = contributorId,
            inputs = inputs
        )
        return SemanticUnitService.CreateResult(statementUnit, children)
    }

    fun update(
        statement: SemanticUnit,
        buildingBlock: BuildingBlock,
        contributorId: ContributorId,
        inputs: Map<ObjectPositionId, ObjectPositionInput>
    ) {
        val subject = when {
            statement !is IdentificationUnit && statement.subject is ResourceId -> {
                repository.findIdentificationUnitBySubjectId(statement.subject)
                    .orElseThrow { IdentificationUnitNotFound(statement.subject) }
            }
            else -> null
        }
        val children = processInputs(
            statementUnit = statement as StatementUnit,
            subject = subject,
            buildingBlock = buildingBlock,
            contributorId = contributorId,
            inputs = inputs
        )
        val result = SemanticUnitService.CreateResult(
            semanticUnit = statement,
            children = children
        )
        repository.saveAll(result.all())
    }

    private fun processInputs(
        statementUnit: StatementUnit,
        subject: StatementUnit?,
        buildingBlock: BuildingBlock,
        contributorId: ContributorId,
        inputs: Map<ObjectPositionId, ObjectPositionInput>
    ): List<SemanticUnitService.CreateResult> {
        val children: MutableList<SemanticUnitService.CreateResult> = mutableListOf()
        (inputs.keys + buildingBlock.objectPositionClasses.keys).forEach {
            val input = inputs[it]
            val objectPosition = buildingBlock.objectPositionClasses[it]
                ?: throw RuntimeException("""Input for object position "$it" is not part of building block "${buildingBlock.id}".""")
            if (input != null) {
                val inputCommand = when (input) {
                    is NewResourceInput -> {
                        if (subject is InstanceIdentificationUnit) {
                            verifyStatementType(statementUnit.type, subject.instanceType, input.instanceType)
                        }
                        // TODO: Move identification unit creation to the object service?
                        val identificationUnitResult = semanticUnitService.createInstanceIdentificationUnit(input, contributorId)
                        children += identificationUnitResult
                        ObjectNodeService.CreateCommand.NewResourcePositionInput(
                            instanceIdentificationUnit = identificationUnitResult.semanticUnit as InstanceIdentificationUnit,
                            classIdentificationUnit = identificationUnitResult.children.singleOrNull()?.semanticUnit as? ClassIdentificationUnit
                                ?: repository.findClassIdentificationUnitByClassUri(input.`class`)
                                    .orElseThrow { IdentificationUnitNotFound(input.`class`) },
                            carriedOverSubjectRangeConstraint = when (statementUnit) {
                                is SubjectConstrainedStatementUnit -> statementUnit.carriedOverSubjectRangeConstraints[objectPosition.id]
                                else -> null
                            }
                        )
                    }
                    is ExistingResourceInput -> {
                        if (subject is InstanceIdentificationUnit && input.resource is ResourceId) {
                            val identificationUnit = repository.findIdentificationUnitBySubjectId(input.resource)
                                .orElseThrow { IdentificationUnitNotFound(input.resource) }
                            if (identificationUnit is InstanceIdentificationUnit) {
                                verifyStatementType(identificationUnit.type, subject.instanceType, identificationUnit.instanceType)
                            }
                        }
                        ObjectNodeService.CreateCommand.ExistingResourcePositionInput(
                            resource = input.resource,
                            carriedOverSubjectRangeConstraint = when (statementUnit) {
                                is SubjectConstrainedStatementUnit -> statementUnit.carriedOverSubjectRangeConstraints[objectPosition.id]
                                else -> null
                            }
                        )
                    }
                    is LiteralObjectPositionInput -> ObjectNodeService.CreateCommand.LiteralPositionInput(input.value)
                }
                val command = ObjectNodeService.CreateCommand(
                    input = inputCommand,
                    objectPosition = objectPosition,
                    contributorId = contributorId
                )
                statementUnit.objectNodes.put(objectNodeService.create(command))
            } else if (objectPosition.required) {
                throw RuntimeException("""Missing required input for object position "$objectPosition" for building block "${buildingBlock.id}".""")
            }
        }
        return children
    }

    private fun verifyStatementType(type: StatementUnit.Type, subject: InstanceIdentificationUnit.Type, `object`: InstanceIdentificationUnit.Type) {
        when (type) {
            StatementUnit.Type.ASSERTIONAL -> {
                if (subject != InstanceIdentificationUnit.Type.NAMED_INDIVIDUAL) {
                    throw RuntimeException("""Subject of assertional statement has to be of type "named individual".""")
                } else if (`object` != InstanceIdentificationUnit.Type.NAMED_INDIVIDUAL) {
                    throw RuntimeException("""Object of assertional statement has to be of type "named individual".""")
                }
            }
            StatementUnit.Type.CONTINGENT -> {
                if (subject != InstanceIdentificationUnit.Type.SOME_INSTANCE) {
                    throw RuntimeException("""Subject of contingent statement has to be of type "some instance".""")
                } else if (`object` != InstanceIdentificationUnit.Type.SOME_INSTANCE) {
                    throw RuntimeException("""Object of contingent statement has to be of type "some instance".""")
                }
            }
            StatementUnit.Type.PROTOTYPICAL -> {
                if (subject != InstanceIdentificationUnit.Type.SOME_INSTANCE) {
                    throw RuntimeException("""Subject of prototypical statement has to be of type "some instance".""")
                } else if (`object` != InstanceIdentificationUnit.Type.SOME_INSTANCE) {
                    throw RuntimeException("""Object of prototypical statement has to be of type "some instance".""")
                }
            }
            StatementUnit.Type.UNIVERSAL -> {
                if (subject != InstanceIdentificationUnit.Type.EVERY_INSTANCE) {
                    throw RuntimeException("""Subject of universal statement has to be of type "every instance".""")
                } else if (`object` != InstanceIdentificationUnit.Type.SOME_INSTANCE) {
                    throw RuntimeException("""Object of universal statement has to be of type "some instance".""")
                }
            }
            StatementUnit.Type.LEXICAL -> {}
        }
    }
}
