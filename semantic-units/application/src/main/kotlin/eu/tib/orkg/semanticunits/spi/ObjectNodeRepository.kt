package eu.tib.orkg.semanticunits.spi

import eu.tib.orkg.semanticunits.domain.ObjectNodeId
import eu.tib.orkg.common.spi.IdRepository

interface ObjectNodeRepository : IdRepository<ObjectNodeId>
