package eu.tib.orkg.semanticunits.domain

import eu.tib.orkg.common.domain.Id

sealed interface SubjectId

class SemanticUnitId(value: String) : Id<SemanticUnitId>(value), SubjectId
class ObjectNodeId(value: String) : Id<ObjectNodeId>(value)
class ResourceId(value: String) : Id<ResourceId>(value), SubjectId
