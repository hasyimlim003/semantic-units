package eu.tib.orkg.semanticunits.domain

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.semanticunits.application.ForbiddenModification
import java.time.LocalDateTime

sealed class SemanticUnit(
    val id: SemanticUnitId,
    val subject: SubjectId,
    val buildingBlockId: BuildingBlockId,
    val createdBy: ContributorId,
    val createdAt: LocalDateTime
) {
    var deletedBy: ContributorId? = null
        set(value) {
            field = checkModifiable(value)
        }

    var deletedAt: LocalDateTime? = null
        set(value) {
            field = checkModifiable(value)
        }

    var isEditable: Boolean = true
        set(value) {
            field = checkModifiable(value)
        }

    private fun <T> checkModifiable(value: T): T = if (isEditable) value else throw ForbiddenModification(id)
}
