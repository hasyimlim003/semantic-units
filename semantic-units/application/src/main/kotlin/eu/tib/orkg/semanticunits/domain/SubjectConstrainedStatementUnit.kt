package eu.tib.orkg.semanticunits.domain

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import java.time.LocalDateTime

class SubjectConstrainedStatementUnit(
    id: SemanticUnitId,
    subject: SubjectId,
    buildingBlockId: BuildingBlockId,
    createdBy: ContributorId,
    createdAt: LocalDateTime,
    type: Type,
    currentNodes: Set<ObjectNode<*>> = mutableSetOf(),
    deletedNodes: Set<ObjectNode<*>> = mutableSetOf(),
    val carriedOverSubjectRangeConstraints: Map<ObjectPositionId, ObjectPositionId> = mutableMapOf()
) : StatementUnit(
    id, subject, buildingBlockId, createdBy, createdAt, currentNodes, deletedNodes, type
)
