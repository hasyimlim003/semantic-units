package eu.tib.orkg.semanticunits.application

import eu.tib.orkg.buildingblocks.api.BuildingBlockInstanceUseCases
import eu.tib.orkg.buildingblocks.api.BuildingBlockUseCases
import eu.tib.orkg.buildingblocks.application.BuildingBlockInstanceNotFound
import eu.tib.orkg.buildingblocks.application.BuildingBlockNotFound
import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.buildingblocks.domain.connection.Association
import eu.tib.orkg.buildingblocks.domain.connection.Link
import eu.tib.orkg.buildingblocks.domain.connection.Reference
import eu.tib.orkg.semanticunits.api.*
import eu.tib.orkg.semanticunits.domain.*
import eu.tib.orkg.semanticunits.domain.identification.ClassIdentificationUnit
import eu.tib.orkg.semanticunits.domain.identification.InstanceIdentificationUnit
import eu.tib.orkg.semanticunits.spi.OntologyRepository
import eu.tib.orkg.semanticunits.spi.SemanticUnitRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.net.URI
import java.util.*

@Service
class SemanticUnitService(
    private val repository: SemanticUnitRepository,
    private val buildingBlockService: BuildingBlockUseCases,
    private val buildingBlockInstanceService: BuildingBlockInstanceUseCases,
    private val statementUnitService: StatementUnitDomainService,
    private val ontologyService: OntologyRepository
): SemanticUnitUseCases {
    override fun create(command: CreateSemanticUnitUseCase.CreateCommand): SemanticUnitId {
        val result = createRecursive(command)
        repository.saveAll(result.all())
        return result.semanticUnit.id
    }

    private fun createRecursive(command: CreateSemanticUnitUseCase.CreateCommand): CreateResult {
        val buildingBlockInstance = buildingBlockInstanceService.findById(command.buildingBlockInstanceId)
            .orElseThrow { BuildingBlockInstanceNotFound(command.buildingBlockInstanceId) }
        val buildingBlock = buildingBlockService.findById(buildingBlockInstance.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(buildingBlockInstance.buildingBlockId) }

        val children: MutableList<CreateResult> = mutableListOf()
        val subject: StatementUnit? = when (command.subject) {
            is ExistingResourceInput -> findSubject(
                subject = command.subject,
                buildingBlock = buildingBlock
            )
            is NewResourceInput -> createSubject(
                subject = command.subject,
                buildingBlock = buildingBlock,
                childInputs = command.children,
                contributorId = command.contributorId,
                children = children
            )
        }

        val unitResult = when (buildingBlock.type) {
            BuildingBlock.Type.STATEMENT -> statementUnitService.create(
                id = command.id,
                subject = subject,
                buildingBlock = buildingBlock,
                contributorId = command.contributorId,
                inputs = command.inputs,
                type = command.type ?: throw RuntimeException("""Missing input for statement type."""),
                carriedOverSubjectRangeConstraints = mapOf() // TODO
            )
        }

        if (buildingBlock.id !in Constants.instanceIdentificationUnitIds) {
            children += createChildren(
                parent = unitResult.semanticUnit,
                buildingBlockInstance = buildingBlockInstance,
                inputs = command.children,
                contributorId = command.contributorId
            )
        }

        return CreateResult(
            semanticUnit = unitResult.semanticUnit,
            children = unitResult.children + children
        )
    }

    private fun findSubject(
        subject: ExistingResourceInput,
        buildingBlock: BuildingBlock,
    ): StatementUnit? = when (subject.resource) {
        is ResourceId -> {
            if (buildingBlock.id in Constants.identificationUnitIds) {
                throw RuntimeException("""Creating an identification unit for an existing resource is not allowed.""")
            }
            val id = subject.resource
            val identificationUnit = repository.findIdentificationUnitBySubjectId(id)
                .orElseThrow { SubjectResourceNotFound(id) }
            if (buildingBlock.subjectConstraint != null) {
                when (identificationUnit) {
                    is ClassIdentificationUnit -> {
                        val classUri = identificationUnit.objectNodes.current[Constants.ClassIdentificationUnit.ObjectPosition.classUri] as StringLiteralObjectNode
                        if (!ontologyService.isSubclassOf(buildingBlock.subjectConstraint!!, URI.create(classUri.value))) {
                            throw RuntimeException(""""${classUri.value}" has to be a subclass of "${buildingBlock.subjectConstraint}".""")
                        }
                    }
                    is InstanceIdentificationUnit -> {
                        val classObjectPositionId = identificationUnit.instanceType.classObjectPositionId
                        val classId = identificationUnit.objectNodes.current[classObjectPositionId]!!.value as ResourceId
                        val classIdentificationUnit = repository.findIdentificationUnitBySubjectId(classId).get() as ClassIdentificationUnit
                        val classUriObjectNode = classIdentificationUnit.objectNodes.current[Constants.ClassIdentificationUnit.ObjectPosition.classUri] as ResourceObjectNode
                        val subjectClassUri = URI.create((classUriObjectNode.value as ResourceId).value)
                        if (!ontologyService.isSubclassOf(buildingBlock.subjectConstraint!!, subjectClassUri)) {
                            throw RuntimeException(""""$subjectClassUri" has to be a subclass of "${buildingBlock.subjectConstraint}".""")
                        }
                    }
                }
            }
            identificationUnit
        }
        is SemanticUnitId -> {
            if (buildingBlock.id in Constants.identificationUnitIds) {
                throw RuntimeException("""Creating an identification unit with a semantic unit as a subject is not allowed.""")
            }
            val id = subject.resource
            val statementUnit = repository.findById(id)
                .orElseThrow { SemanticUnitNotFound(id) }
            if (statementUnit !is StatementUnit) {
                throw RuntimeException("""Semantic unit "${statementUnit.id}" cannot be used as a subject because it is not a statement unit.""")
            }
            if (buildingBlock.subjectConstraint != null) {
                TODO("implement")
            }
            statementUnit
        }
    }

    private fun createSubject(
        subject: NewResourceInput,
        buildingBlock: BuildingBlock,
        childInputs: List<SemanticUnitInput>,
        contributorId: ContributorId,
        children: MutableList<CreateResult>
    ): StatementUnit? = when (buildingBlock.id) {
        in Constants.instanceIdentificationUnitIds -> {
            repository.findClassIdentificationUnitByClassUri(subject.`class`).orElseGet {
                val result = createRecursive(
                    CreateSemanticUnitUseCase.CreateCommand(
                        subject = subject,
                        contributorId = contributorId,
                        buildingBlockInstanceId = subject.instanceType.buildingBlockInstanceId,
                        type = StatementUnit.Type.LEXICAL,
                        inputs = childInputs[0].inputs, // TODO: validate?
                        children = listOf()
                    )
                )
                children += result
                result.semanticUnit as ClassIdentificationUnit
            }
        }
        Constants.ClassIdentificationUnit.buildingBlockId -> null
        else -> {
            if (buildingBlock.subjectConstraint != null && !ontologyService.isSubclassOf(buildingBlock.subjectConstraint!!, subject.`class`)) {
                throw RuntimeException(""""${subject.`class`}" has to be a subclass of "${buildingBlock.subjectConstraint}".""")
            }
            val result = createInstanceIdentificationUnit(subject, contributorId)
            children += result
            result.semanticUnit as StatementUnit
        }
    }

    private fun createChildren(
        parent: SemanticUnit,
        buildingBlockInstance: BuildingBlockInstance,
        inputs: List<SemanticUnitInput>,
        contributorId: ContributorId
    ): List<CreateResult> {
        val connections = buildingBlockInstance.allConnections
        val children: MutableList<CreateResult> = mutableListOf()
        val connectionCounts: MutableMap<ConnectionId, Int> = mutableMapOf()

        inputs.forEach {
            if (it.connectionId !in connections) {
                throw RuntimeException("""Invalid connection "${it.connectionId}".""")
            }
            val connection = connections[it.connectionId]!!
            if (connectionCounts[connection.id] == connection.maxCount) {
                throw RuntimeException("""Cannot instantiate connection "${connection.id}" more than ${connection.maxCount} times.""")
            }
            val childSubject = when (connection) {
                is Link -> {
                    if (parent !is StatementUnit) {
                        throw RuntimeException("""Source semantic unit for link "${connection.id}" is not of type statement unit. This is a bug!""")
                    }
                    val node = parent.objectNodes.current[connection.useAsSubject]!!
                    if (node !is ResourceObjectNode) {
                        throw RuntimeException("""Object node "${node.id}" for object position "${connection.useAsSubject}" is not of type resource. This is a bug!""")
                    }
                    ExistingResourceInput(node.value)
                }
                is Association -> {
                    if (parent.subject !is ResourceId) {
                        throw RuntimeException("""Subject of source semantic unit for association "${connection.id}" is not of type resource. This is a bug!""")
                    }
                    ExistingResourceInput(parent.subject)
                }
                is Reference -> ExistingResourceInput(parent.id)
            }
            val child = createRecursive(
                CreateSemanticUnitUseCase.CreateCommand(
                    subject = childSubject,
                    contributorId = contributorId,
                    buildingBlockInstanceId = connection.targetBuildingBlockInstance,
                    type = it.type,
                    inputs = it.inputs,
                    children = it.children
                )
            )
            connectionCounts.compute(it.connectionId) { _, value -> value?.plus(1) ?: 1 }
            children += child
        }

        connections.values.forEach {
            if (it.id !in connectionCounts && it.minCount > 0) {
                throw RuntimeException("""Cannot instantiate "${buildingBlockInstance.id}" for building block "${buildingBlockInstance.buildingBlockId}" because input for connection "${it.id}" is missing.""")
            }
        }

        return children
    }

    override fun findById(semanticUnitId: SemanticUnitId): Optional<SemanticUnit> =
        repository.findById(semanticUnitId)

    override fun findAll(pageable: Pageable): Page<SemanticUnit> =
        repository.findAll(pageable)

    override fun update(command: UpdateSemanticUnitUseCase.UpdateCommand) {
        val statement = repository.findById(command.id)
            .orElseThrow { SemanticUnitNotFound(command.id) }
        if (!statement.isEditable) throw ForbiddenModification(statement.id)
        val buildingBlock = buildingBlockService.findById(statement.buildingBlockId)
            .orElseThrow { BuildingBlockNotFound(statement.buildingBlockId) }

        when (buildingBlock.type) {
            BuildingBlock.Type.STATEMENT -> {
                statementUnitService.update(statement, buildingBlock, command.contributorId, command.inputs)
            }
        }
    }

    override fun deleteAll() = repository.deleteAll()

    internal fun createInstanceIdentificationUnit(
        subject: NewResourceInput,
        contributorId: ContributorId
    ): CreateResult {
        val term = ontologyService.findByURI(subject.`class`)
            .orElseThrow { TermNotFound(subject.`class`) }
        return createRecursive(
            CreateSemanticUnitUseCase.CreateCommand(
                subject = subject,
                contributorId = contributorId,
                buildingBlockInstanceId = subject.instanceType.buildingBlockInstanceId,
                type = StatementUnit.Type.LEXICAL,
                inputs = mapOf(
                    subject.instanceType.classObjectPositionId to subject,
                    subject.instanceType.labelObjectPositionId to LiteralObjectPositionInput(
                        value = subject.label ?: term.label
                    )
                ),
                children = listOf(
                    SemanticUnitInput(
                        connectionId = subject.instanceType.classIdentificationUnitLinkId,
                        type = StatementUnit.Type.LEXICAL,
                        inputs = mapOf(
                            Constants.ClassIdentificationUnit.ObjectPosition.classUri to LiteralObjectPositionInput(term.uri.toString()),
                            Constants.ClassIdentificationUnit.ObjectPosition.label to LiteralObjectPositionInput(term.label),
                            Constants.ClassIdentificationUnit.ObjectPosition.ontology to LiteralObjectPositionInput(term.ontology)
                        ),
                        children = emptyList()
                    )
                )
            )
        )
    }

    data class CreateResult(
        val semanticUnit: SemanticUnit,
        val children: List<CreateResult>
    ) {
        fun all(): List<SemanticUnit> = children.map { it.all() }.flatten() + semanticUnit
    }
}
