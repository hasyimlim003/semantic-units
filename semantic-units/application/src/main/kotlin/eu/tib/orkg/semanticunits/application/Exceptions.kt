package eu.tib.orkg.semanticunits.application

import eu.tib.orkg.semanticunits.domain.ResourceId
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.SubjectId
import eu.tib.orkg.common.application.SimpleMessageException
import org.springframework.http.HttpStatus
import java.net.URI

class SemanticUnitNotFound(id: SemanticUnitId) :
    SimpleMessageException(HttpStatus.NOT_FOUND, """Semantic unit "$id" not found.""")

class IdentificationUnitNotFound : SimpleMessageException {
    constructor(id: ResourceId) : super(HttpStatus.NOT_FOUND, """Identification unit for resource "$id" not found.""")
    constructor(uri: URI) : super(HttpStatus.NOT_FOUND, """Identification unit for uri "$uri" not found.""")
}

class SemanticUnitAlreadyExists(id: SemanticUnitId) :
    SimpleMessageException(HttpStatus.FORBIDDEN, """Semantic unit "$id" already exists.""")

class SubjectResourceNotFound(id: SubjectId) :
    SimpleMessageException(HttpStatus.NOT_FOUND, """Subject resource "$id" not found.""")

class ForbiddenModification(id: SemanticUnitId) :
    SimpleMessageException(HttpStatus.FORBIDDEN, """Semantic unit "$id" cannot be modified.""")

class TermNotFound(uri: URI) :
    SimpleMessageException(HttpStatus.FORBIDDEN, """Term "$uri" not found.""")
