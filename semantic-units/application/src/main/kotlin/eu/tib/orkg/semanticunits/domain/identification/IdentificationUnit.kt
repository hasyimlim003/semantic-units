package eu.tib.orkg.semanticunits.domain.identification

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.semanticunits.domain.ObjectNode
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.StatementUnit
import eu.tib.orkg.semanticunits.domain.SubjectId
import java.time.LocalDateTime

sealed class IdentificationUnit(
    id: SemanticUnitId,
    subject: SubjectId,
    buildingBlockId: BuildingBlockId,
    createdBy: ContributorId,
    createdAt: LocalDateTime,
    currentNodes: Set<ObjectNode<*>>,
    deletedNodes: Set<ObjectNode<*>>
) : StatementUnit(
    id, subject, buildingBlockId, createdBy, createdAt, currentNodes, deletedNodes, Type.LEXICAL
)
