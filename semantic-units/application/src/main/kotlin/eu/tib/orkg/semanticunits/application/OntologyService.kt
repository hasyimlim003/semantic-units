package eu.tib.orkg.semanticunits.application

import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.semanticunits.spi.OntologyRepository
import org.springframework.stereotype.Service
import java.net.URI
import java.util.*

@Service
class OntologyService(
    private val repository: OntologyRepository
) {
    fun findByURI(uri: URI): Optional<OntologyRepository.Term> =
        repository.findByURI(uri)

    fun isSubclassOf(constraint: URI, uri: URI): Boolean =
        repository.isSubclassOf(constraint, uri)

    fun isSubclassOf(constraint: BuildingBlockId, buildingBlockId: BuildingBlockId): Boolean =
        repository.isSubclassOf(constraint, buildingBlockId)
}
