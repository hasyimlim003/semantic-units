package eu.tib.orkg.semanticunits.domain.identification

import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.semanticunits.domain.ObjectNode
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.SubjectId
import java.time.LocalDateTime

class InstanceIdentificationUnit(
    id: SemanticUnitId,
    subject: SubjectId,
    createdBy: ContributorId,
    createdAt: LocalDateTime,
    currentNodes: Set<ObjectNode<*>> = mutableSetOf(),
    deletedNodes: Set<ObjectNode<*>> = mutableSetOf(),
    val instanceType: Type
) : IdentificationUnit(
    id, subject, instanceType.buildingBlockId, createdBy, createdAt, currentNodes, deletedNodes
) {
    enum class Type(
        val buildingBlockId: BuildingBlockId,
        val buildingBlockInstanceId: BuildingBlockInstanceId,
        val classIdentificationUnitLinkId: LinkId,
        val classObjectPositionId: ObjectPositionId,
        val labelObjectPositionId: ObjectPositionId
    ) {
        NAMED_INDIVIDUAL(
            buildingBlockId = Constants.NamedIndividualIdentificationUnit.buildingBlockId,
            buildingBlockInstanceId = Constants.NamedIndividualIdentificationUnit.buildingBlockInstanceId,
            classIdentificationUnitLinkId = Constants.NamedIndividualIdentificationUnit.classIdentificationUnitLinkId,
            classObjectPositionId = Constants.NamedIndividualIdentificationUnit.ObjectPosition.`class`,
            labelObjectPositionId = Constants.NamedIndividualIdentificationUnit.ObjectPosition.label
        ),
        SOME_INSTANCE(
            buildingBlockId = Constants.SomeInstanceIdentificationUnit.buildingBlockId,
            buildingBlockInstanceId = Constants.SomeInstanceIdentificationUnit.buildingBlockInstanceId,
            classIdentificationUnitLinkId = Constants.SomeInstanceIdentificationUnit.classIdentificationUnitLinkId,
            classObjectPositionId = Constants.SomeInstanceIdentificationUnit.ObjectPosition.`class`,
            labelObjectPositionId = Constants.SomeInstanceIdentificationUnit.ObjectPosition.label
        ),
        EVERY_INSTANCE(
            buildingBlockId = Constants.EveryInstanceIdentificationUnit.buildingBlockId,
            buildingBlockInstanceId = Constants.EveryInstanceIdentificationUnit.buildingBlockInstanceId,
            classIdentificationUnitLinkId = Constants.EveryInstanceIdentificationUnit.classIdentificationUnitLinkId,
            classObjectPositionId = Constants.EveryInstanceIdentificationUnit.ObjectPosition.`class`,
            labelObjectPositionId = Constants.EveryInstanceIdentificationUnit.ObjectPosition.label
        )
    }
}
