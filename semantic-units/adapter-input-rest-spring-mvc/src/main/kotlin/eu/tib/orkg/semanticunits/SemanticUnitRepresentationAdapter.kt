package eu.tib.orkg.semanticunits

import eu.tib.orkg.semanticunits.api.InstanceIdentificationStatementUnitRepresentation
import eu.tib.orkg.semanticunits.api.SemanticUnitRepresentation
import eu.tib.orkg.semanticunits.api.StatementUnitRepresentation
import eu.tib.orkg.semanticunits.api.SubjectConstrainedStatementUnitRepresentation
import eu.tib.orkg.semanticunits.domain.SemanticUnit
import eu.tib.orkg.semanticunits.domain.StatementUnit
import eu.tib.orkg.semanticunits.domain.SubjectConstrainedStatementUnit
import eu.tib.orkg.semanticunits.domain.identification.InstanceIdentificationUnit
import org.springframework.data.domain.Page
import java.util.*

interface SemanticUnitRepresentationAdapter : ObjectNodeRepresentationAdapter {
    fun Optional<SemanticUnit>.mapToSemanticUnitRepresentation(): Optional<SemanticUnitRepresentation> =
        map { it.toSemanticUnitRepresentation() }

    fun Page<SemanticUnit>.mapToSemanticUnitRepresentation(): Page<SemanticUnitRepresentation> =
        map { it.toSemanticUnitRepresentation() }

    private fun SemanticUnit.toSemanticUnitRepresentation(): SemanticUnitRepresentation =
        when (this) {
            is InstanceIdentificationUnit -> {
                InstanceIdentificationStatementUnitRepresentation(
                    id = id,
                    subject = subject,
                    buildingBlockId = buildingBlockId,
                    createdBy = createdBy,
                    createdAt = createdAt,
                    deletedBy = deletedBy,
                    deletedAt = deletedAt,
                    isEditable = isEditable,
                    type = type,
                    instanceType = instanceType,
                    objectNodes = objectNodes.all.map { it.toObjectNodeRepresentation() }.toSet()
                )
            }
            is SubjectConstrainedStatementUnit -> {
                SubjectConstrainedStatementUnitRepresentation(
                    id = id,
                    subject = subject,
                    buildingBlockId = buildingBlockId,
                    createdBy = createdBy,
                    createdAt = createdAt,
                    deletedBy = deletedBy,
                    deletedAt = deletedAt,
                    isEditable = isEditable,
                    type = type,
                    carriedOverSubjectRangeConstraints = carriedOverSubjectRangeConstraints,
                    objectNodes = objectNodes.all.map { it.toObjectNodeRepresentation() }.toSet()
                )
            }
            is StatementUnit -> {
                StatementUnitRepresentation(
                    id = id,
                    subject = subject,
                    buildingBlockId = buildingBlockId,
                    createdBy = createdBy,
                    createdAt = createdAt,
                    deletedBy = deletedBy,
                    deletedAt = deletedAt,
                    isEditable = isEditable,
                    type = type,
                    objectNodes = objectNodes.all.map { it.toObjectNodeRepresentation() }.toSet()
                )
            }
        }
}
