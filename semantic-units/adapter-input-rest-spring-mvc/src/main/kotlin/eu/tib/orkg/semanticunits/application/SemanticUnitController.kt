package eu.tib.orkg.semanticunits.application

import com.fasterxml.jackson.annotation.JsonProperty
import eu.tib.orkg.auth.api.UserUseCases
import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.semanticunits.SemanticUnitRepresentationAdapter
import eu.tib.orkg.semanticunits.api.*
import eu.tib.orkg.semanticunits.domain.ResourceId
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.StatementUnit
import eu.tib.orkg.semanticunits.domain.identification.InstanceIdentificationUnit
import jakarta.validation.constraints.Pattern
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.security.Principal

@RestController
@RequestMapping("/api/semantic-units", produces = [MediaType.APPLICATION_JSON_VALUE])
class SemanticUnitController(
    private val service: SemanticUnitUseCases,
    private val userService: UserUseCases
) : SemanticUnitRepresentationAdapter {
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun create(
        @RequestBody @Validated request: CreateSemanticUnitRequest,
        principal: Principal?,
        uriComponentsBuilder: UriComponentsBuilder
    ): ResponseEntity<SemanticUnitRepresentation> {
        val user = userService.authenticateUser(principal)
        val command = request.toCreateCommand(ContributorId(user.id))
        val id = service.create(command)
        val location = uriComponentsBuilder
            .path("api/semantic-units/{id}") // TODO: check if path needs a leading '/'
            .buildAndExpand(id)
            .toUri()
        return created(location).body(service.findById(id).mapToSemanticUnitRepresentation().get())
    }

    @PostMapping("/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun update(
        @PathVariable id: SemanticUnitId,
        @RequestBody @Validated request: UpdateSemanticUnitRequest,
        principal: Principal?,
        uriComponentsBuilder: UriComponentsBuilder
    ): SemanticUnitRepresentation {
        val user = userService.authenticateUser(principal)
        val command = request.toUpdateCommand(id, ContributorId(user.id))
        service.update(command)
        return service.findById(id).mapToSemanticUnitRepresentation().get()
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: SemanticUnitId): SemanticUnitRepresentation =
        service.findById(id)
            .mapToSemanticUnitRepresentation()
            .orElseThrow { SemanticUnitNotFound(id) }

    @GetMapping
    fun findAll(pageable: Pageable): Page<SemanticUnitRepresentation> =
        service.findAll(pageable).mapToSemanticUnitRepresentation()

    data class CreateSemanticUnitRequest(
        val subject: SemanticUnitInputRequest.ResourceInputRequest,
        @JsonProperty("building_block_instance_id")
        val buildingBlockInstanceId: BuildingBlockInstanceId,
        val type: StatementUnit.Type?,
        val inputs: Map<ObjectPositionId, SemanticUnitInputRequest.ObjectPositionInputRequest>,
        val children: List<SemanticUnitInputRequest>
    ) {
        fun toCreateCommand(contributorId: ContributorId) = CreateSemanticUnitUseCase.CreateCommand(
            subject = subject.toCreateCommand(),
            contributorId = contributorId,
            buildingBlockInstanceId = buildingBlockInstanceId,
            type = type,
            inputs = inputs.mapValues { it.value.toCreateCommand() },
            children = children.map { it.toCreateCommand() }
        )
    }

    data class UpdateSemanticUnitRequest(
        val inputs: Map<ObjectPositionId, SemanticUnitInputRequest.ObjectPositionInputRequest>,
        val children: List<SemanticUnitInputRequest>
    ) {
        fun toUpdateCommand(id: SemanticUnitId, contributorId: ContributorId) = UpdateSemanticUnitUseCase.UpdateCommand(
            id = id,
            contributorId = contributorId,
            inputs = inputs.mapValues { it.value.toCreateCommand() },
            children = children.map { it.toCreateCommand() }
        )
    }

    data class SemanticUnitInputRequest(
        @JsonProperty("connection_id")
        val connectionId: ConnectionId,
        val type: StatementUnit.Type?,
        val inputs: Map<ObjectPositionId, ObjectPositionInputRequest>,
        val children: List<SemanticUnitInputRequest>
    ) {
        sealed interface ObjectPositionInputRequest {
            fun toCreateCommand(): ObjectPositionInput
        }

        sealed interface ResourceInputRequest : ObjectPositionInputRequest {
            override fun toCreateCommand(): ResourceInput
        }

        data class ExistingResourceInputRequest(
            val resource: ResourceId
        ) : ResourceInputRequest {
            override fun toCreateCommand() = ExistingResourceInput(resource)
        }

        data class NewResourceInputRequest(
            val `class`: URI,
            @Pattern(regexp = """^(?!\s*\$)[\s\S]+""") // Allow null and not-blank values
            val label: String?,
            val type: InstanceIdentificationUnit.Type
        ) : ResourceInputRequest {
            override fun toCreateCommand() = NewResourceInput(`class`, label, type)
        }

        data class StatementUnitResourceInputRequest(
            val statement: SemanticUnitId
        ) : ResourceInputRequest {
            override fun toCreateCommand() = ExistingResourceInput(statement)
        }

        data class LiteralObjectInputRequest(
            val value: String
        ) : ObjectPositionInputRequest {
            override fun toCreateCommand() = LiteralObjectPositionInput(value)
        }

        fun toCreateCommand(): SemanticUnitInput = SemanticUnitInput(
            connectionId = connectionId,
            type = type,
            inputs = inputs.mapValues { it.value.toCreateCommand() },
            children = children.map { it.toCreateCommand() }
        )
    }
}
