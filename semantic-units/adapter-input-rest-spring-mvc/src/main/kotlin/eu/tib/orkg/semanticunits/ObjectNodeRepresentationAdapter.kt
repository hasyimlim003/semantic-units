package eu.tib.orkg.semanticunits

import eu.tib.orkg.semanticunits.api.ObjectNodeRepresentation
import eu.tib.orkg.semanticunits.domain.ObjectNode
import java.util.*

interface ObjectNodeRepresentationAdapter {
    fun Optional<ObjectNode<*>>.mapToObjectNodeRepresentation(): Optional<ObjectNodeRepresentation<*>> =
        map { it.toObjectNodeRepresentation() }

    fun <T> ObjectNode<T>.toObjectNodeRepresentation(): ObjectNodeRepresentation<T> =
        ObjectNodeRepresentation(
            id = id,
            objectPositionClass = objectPositionClass,
            createdBy = createdBy,
            createdAt = createdAt,
            value = value
        )
}
