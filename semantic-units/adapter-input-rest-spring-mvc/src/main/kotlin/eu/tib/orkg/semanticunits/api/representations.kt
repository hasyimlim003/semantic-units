package eu.tib.orkg.semanticunits.api

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import eu.tib.orkg.buildingblocks.domain.BuildingBlockId
import eu.tib.orkg.buildingblocks.domain.ContributorId
import eu.tib.orkg.buildingblocks.domain.ObjectPositionId
import eu.tib.orkg.semanticunits.domain.ObjectNodeId
import eu.tib.orkg.semanticunits.domain.SemanticUnitId
import eu.tib.orkg.semanticunits.domain.StatementUnit
import eu.tib.orkg.semanticunits.domain.SubjectId
import eu.tib.orkg.semanticunits.domain.identification.InstanceIdentificationUnit
import java.time.LocalDateTime

sealed interface SemanticUnitRepresentation {
    val id: SemanticUnitId
    val subject: SubjectId
    @get:JsonProperty("building_block_id")
    val buildingBlockId: BuildingBlockId
    @get:JsonProperty("created_by")
    val createdBy: ContributorId
    @get:JsonProperty("created_at")
    val createdAt: LocalDateTime
    @get:JsonProperty("deleted_by")
    @get:JsonInclude(JsonInclude.Include.NON_NULL)
    val deletedBy: ContributorId?
    @get:JsonProperty("deleted_at")
    @get:JsonInclude(JsonInclude.Include.NON_NULL)
    val deletedAt: LocalDateTime?
    @get:JsonProperty("is_editable")
    val isEditable: Boolean
}

open class StatementUnitRepresentation(
    override val id: SemanticUnitId,
    override val subject: SubjectId,
    override val buildingBlockId: BuildingBlockId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val deletedBy: ContributorId?,
    override val deletedAt: LocalDateTime?,
    override val isEditable: Boolean,
    val type: StatementUnit.Type,
    @JsonProperty("object_nodes")
    val objectNodes: Set<ObjectNodeRepresentation<*>>
) : SemanticUnitRepresentation

class SubjectConstrainedStatementUnitRepresentation(
    override val id: SemanticUnitId,
    override val subject: SubjectId,
    override val buildingBlockId: BuildingBlockId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val deletedBy: ContributorId?,
    override val deletedAt: LocalDateTime?,
    override val isEditable: Boolean,
    type: StatementUnit.Type,
    objectNodes: Set<ObjectNodeRepresentation<*>>,
    @JsonProperty("carried_over_subject_range_constraints")
    val carriedOverSubjectRangeConstraints: Map<ObjectPositionId, ObjectPositionId>,
) : StatementUnitRepresentation(
    id = id,
    subject = subject,
    buildingBlockId = buildingBlockId,
    createdBy = createdBy,
    createdAt = createdAt,
    deletedBy = deletedBy,
    deletedAt = deletedAt,
    isEditable = isEditable,
    type = type,
    objectNodes = objectNodes
)

class InstanceIdentificationStatementUnitRepresentation(
    override val id: SemanticUnitId,
    override val subject: SubjectId,
    override val buildingBlockId: BuildingBlockId,
    override val createdBy: ContributorId,
    override val createdAt: LocalDateTime,
    override val deletedBy: ContributorId?,
    override val deletedAt: LocalDateTime?,
    override val isEditable: Boolean,
    type: StatementUnit.Type,
    objectNodes: Set<ObjectNodeRepresentation<*>>,
    @JsonProperty("instance_type")
    val instanceType: InstanceIdentificationUnit.Type
) : StatementUnitRepresentation(
    id = id,
    subject = subject,
    buildingBlockId = buildingBlockId,
    createdBy = createdBy,
    createdAt = createdAt,
    deletedBy = deletedBy,
    deletedAt = deletedAt,
    isEditable = isEditable,
    type = type,
    objectNodes = objectNodes
)

data class ObjectNodeRepresentation<T>(
    val id: ObjectNodeId,
    @get:JsonProperty("object_position_class")
    val objectPositionClass: ObjectPositionId,
    @get:JsonProperty("created_by")
    val createdBy: ContributorId,
    @get:JsonProperty("created_at")
    val createdAt: LocalDateTime,
    val value: T
)
