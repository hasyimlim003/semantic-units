package eu.tib.orkg

import eu.tib.orkg.buildingblocks.domain.*
import eu.tib.orkg.buildingblocks.domain.connection.Link
import eu.tib.orkg.buildingblocks.spi.BuildingBlockInstanceRepository
import eu.tib.orkg.buildingblocks.spi.BuildingBlockRepository
import eu.tib.orkg.buildingblocks.spi.ObjectPositionRepository
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class InitialDataSetup(
    private val buildingBlockRepository: BuildingBlockRepository,
    private val buildingBlockInstanceRepository: BuildingBlockInstanceRepository,
    private val objectPositionRepository: ObjectPositionRepository
) : ApplicationRunner {
    override fun run(args: ApplicationArguments?) {
        val namedIndividualIdentificationUnitBuildingBlock = BuildingBlock(
            id = Constants.NamedIndividualIdentificationUnit.buildingBlockId,
            label = "Named Individual Identification Unit",
            description = "",
            type = BuildingBlock.Type.STATEMENT,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            subjectConstraint = null,
            objectPositionClasses = setOf(
                StringObjectPosition(
                    id = Constants.NamedIndividualIdentificationUnit.ObjectPosition.label,
                    label = "Label Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                ),
                ResourceObjectPosition(
                    id = Constants.NamedIndividualIdentificationUnit.ObjectPosition.`class`,
                    label = "Class Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                )
            ).associateBy { it.id }
        )
        val someInstanceIdentificationUnitBuildingBlock = BuildingBlock(
            id = Constants.SomeInstanceIdentificationUnit.buildingBlockId,
            label = "Some Instance Identification Unit",
            description = "",
            type = BuildingBlock.Type.STATEMENT,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            subjectConstraint = null,
            objectPositionClasses = setOf(
                StringObjectPosition(
                    id = Constants.SomeInstanceIdentificationUnit.ObjectPosition.label,
                    label = "Label Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                ),
                ResourceObjectPosition(
                    id = Constants.SomeInstanceIdentificationUnit.ObjectPosition.`class`,
                    label = "Class Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                )
            ).associateBy { it.id }
        )
        val everyInstanceIdentificationUnitBuildingBlock = BuildingBlock(
            id = Constants.EveryInstanceIdentificationUnit.buildingBlockId,
            label = "Every Instance Identification Unit",
            description = "Class Object Position",
            type = BuildingBlock.Type.STATEMENT,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            subjectConstraint = null,
            objectPositionClasses = setOf(
                StringObjectPosition(
                    id = Constants.EveryInstanceIdentificationUnit.ObjectPosition.label,
                    label = "Label Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                ),
                ResourceObjectPosition(
                    id = Constants.EveryInstanceIdentificationUnit.ObjectPosition.`class`,
                    label = "",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                )
            ).associateBy { it.id }
        )
        val classIdentificationUnitBuildingBlock = BuildingBlock(
            id = Constants.ClassIdentificationUnit.buildingBlockId,
            label = "Class Identification Unit",
            description = "",
            type = BuildingBlock.Type.STATEMENT,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            subjectConstraint = null,
            objectPositionClasses = setOf(
                StringObjectPosition(
                    id = Constants.ClassIdentificationUnit.ObjectPosition.label,
                    label = "Label Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                ),
                StringObjectPosition(
                    id = Constants.ClassIdentificationUnit.ObjectPosition.classUri,
                    label = "Term id Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                ),
                StringObjectPosition(
                    id = Constants.ClassIdentificationUnit.ObjectPosition.ontology,
                    label = "Ontology Object Position",
                    required = true,
                    constraint = null,
                    createdBy = ContributorId.SYSTEM,
                    createdAt = LocalDateTime.MIN
                )
            ).associateBy { it.id }
        )
        setOf(
            namedIndividualIdentificationUnitBuildingBlock,
            someInstanceIdentificationUnitBuildingBlock,
            everyInstanceIdentificationUnitBuildingBlock,
            classIdentificationUnitBuildingBlock
        ).forEach { buildingBlock ->
            buildingBlock.objectPositionClasses.values.forEach { objectPosition ->
                objectPositionRepository.save(objectPosition)
            }
            buildingBlockRepository.save(buildingBlock)
        }

        val classIdentificationUnitBuildingBlockInstance = BuildingBlockInstance(
            id = Constants.ClassIdentificationUnit.buildingBlockInstanceId,
            buildingBlockId = Constants.ClassIdentificationUnit.buildingBlockId,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN
        )
        buildingBlockInstanceRepository.save(classIdentificationUnitBuildingBlockInstance)

        val niiu2ciuLink = Link(
            id = Constants.NamedIndividualIdentificationUnit.classIdentificationUnitLinkId,
            targetBuildingBlockInstance = classIdentificationUnitBuildingBlockInstance.id,
            minCount = 1,
            maxCount = 1,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            useAsSubject = Constants.NamedIndividualIdentificationUnit.ObjectPosition.`class`
        )
        val siiu2ciuLink = Link(
            id = Constants.SomeInstanceIdentificationUnit.classIdentificationUnitLinkId,
            targetBuildingBlockInstance = classIdentificationUnitBuildingBlockInstance.id,
            minCount = 1,
            maxCount = 1,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            useAsSubject = Constants.SomeInstanceIdentificationUnit.ObjectPosition.`class`
        )
        val eiiu2ciuLink = Link(
            id = Constants.EveryInstanceIdentificationUnit.classIdentificationUnitLinkId,
            targetBuildingBlockInstance = classIdentificationUnitBuildingBlockInstance.id,
            minCount = 1,
            maxCount = 1,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            useAsSubject = Constants.EveryInstanceIdentificationUnit.ObjectPosition.`class`
        )

        val namedIndividualIdentificationUnitBuildingBlockInstance = BuildingBlockInstance(
            id = Constants.NamedIndividualIdentificationUnit.buildingBlockInstanceId,
            buildingBlockId = Constants.NamedIndividualIdentificationUnit.buildingBlockId,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            links = mutableMapOf(niiu2ciuLink.id to niiu2ciuLink)
        )
        buildingBlockInstanceRepository.save(namedIndividualIdentificationUnitBuildingBlockInstance)

        val someInstanceIdentificationUnitBuildingBlockInstance = BuildingBlockInstance(
            id = Constants.SomeInstanceIdentificationUnit.buildingBlockInstanceId,
            buildingBlockId = Constants.SomeInstanceIdentificationUnit.buildingBlockId,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            links = mutableMapOf(siiu2ciuLink.id to siiu2ciuLink)
        )
        buildingBlockInstanceRepository.save(someInstanceIdentificationUnitBuildingBlockInstance)

        val everyInstanceIdentificationUnitBuildingBlockInstance = BuildingBlockInstance(
            id = Constants.EveryInstanceIdentificationUnit.buildingBlockInstanceId,
            buildingBlockId = Constants.EveryInstanceIdentificationUnit.buildingBlockId,
            createdBy = ContributorId.SYSTEM,
            createdAt = LocalDateTime.MIN,
            links = mutableMapOf(eiiu2ciuLink.id to eiiu2ciuLink)
        )
        buildingBlockInstanceRepository.save(everyInstanceIdentificationUnitBuildingBlockInstance)
        println("Generated initial data")
    }
}
