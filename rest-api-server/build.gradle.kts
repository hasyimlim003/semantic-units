// JVM Test Suite is still incubating, but expected to be stable soon, so disabling the warning.
@file:Suppress("UnstableApiUsage")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
    kotlin("jvm") version "1.8.10"
    kotlin("plugin.spring") version "1.8.10"

    id("org.springframework.boot") version "3.0.3"
    id("io.spring.dependency-management") version "1.1.0"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":building-blocks:application"))
    implementation(project(":semantic-units:application"))
    implementation(project(":common:application"))

    implementation(project(":building-blocks:adapter-input-rest-spring-mvc"))
    implementation(project(":semantic-units:adapter-input-rest-spring-mvc"))

//    implementation(project(":building-blocks:adapter-output-spring-data-neo4j"))
//    implementation(project(":semantic-units:adapter-output-spring-data-neo4j"))

    implementation(project(":building-blocks:adapter-output-in-memory"))
    implementation(project(":semantic-units:adapter-output-in-memory"))

    implementation(project(":identity-management:application"))
    implementation(project(":identity-management:adapter-output-in-memory"))

    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
}

testing {
    suites {
        // Configure the built-in test suite
        val test by getting(JvmTestSuite::class) {
            useKotlinTest("1.8.10")

            dependencies {
                // Use newer version of JUnit Engine for Kotlin Test
                implementation("org.junit.jupiter:junit-jupiter-engine:5.9.1")
            }
        }
    }
}

tasks {
    named("bootRun", BootRun::class.java).configure {
        args("--spring.profiles.active=development")
    }

    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "17"
        }
    }
}

springBoot {
    buildInfo()
}

normalization {
    runtimeClasspath {
        // This only affects build cache key calculation. The file will be included in the build.
        ignore("**/build-info.properties")
    }
}
